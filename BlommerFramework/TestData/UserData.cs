﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BlommerFramework.TestData
{
    public class UserData
    {
        public string Key { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string InvalidUsername { get; set; }
        public string InvalidPassword { get; set; }
        public string SKU { get; set; }
        public string Description { get; set; }
        public string LOT { get; set; }
        public string FormulaID { get; set; }
        public string Building { get; set; }
        public string Location { get; set; }
        public string Container { get; set; }
        public string InvalidBuilding { get; set; }
        public string InvalidLocation { get; set; }
        public string ClearFilterLot { get; set; }
        public string ClearFilterFormulaID { get; set; }
        public string AcessReworkPageLot { get; set; }
        public string RankingLotId { get;  set; }
        public string Ranking { get;  set; }

        public string ReworkRanking { get;  set; }
        public string BatchID { get; set; }

        public string FormulaIDValue { get;  set; }

        public string ReworkContainer { get; set; }
        public string StabilizingBatchId { get; set; }

        public string StartDate { get; set; }
        public string CompletionDate { get; set; }
        public string LotWeight { get; set; }
        public string SkuWeight { get; set; }
        public string Pound { get; set; }


    }
}