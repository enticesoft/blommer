﻿using BlommerFramework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;


namespace BlommerFramework
{
    
    public class OverviewPage
    {
      
        IWebDriver driver;

        [FindsBy(How = How.Id, Using = "GridView_btnSelect_0")]
        public IWebElement SelectBtn;
        [FindsBy(How = How.Id, Using = "DetailsViewMaintTitle")]
        public IWebElement BatchDetails;
        [FindsBy(How = How.Id, Using = "Create")]
        public IWebElement CreateEditBatchBtn;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_Back")]
        public IWebElement BackBtn;
        [FindsBy(How = How.XPath, Using = "//span[contains(text(),'Batch Overview')]")]
        public IWebElement BatchOverviewPage;
        [FindsBy(How = How.Id, Using = "UCPageHeader1_PageBackLink")]
        public IWebElement MainMenuBtn;
        [FindsBy(How = How.Id, Using = "GridView_Button4_0")]
        public IWebElement SelectBtnForRefining;


        public void Goto()
        {
            Pages.Login.LogIn("login");
            Browser.Goto("BatchSystem/0_Supervisor/Batch.aspx");
        }

        public bool IsAt()
        {
            
            return Browser.Title.Contains("Overview - Batch Process");
        }

        public bool BatchMaintanancePage()
        {
            return Browser.Title.Contains("Batch Maintenance - Batch Process");
        }

        public void DetailsBatchInfo()
        {
            SelectBtn.Click();
        }
        public void RefiningDetailsBatchInfo()
        {
            SelectBtnForRefining.Click();
        }
        public void CreateEditBatch()
        {
            CreateEditBatchBtn.Click();
        }
        public void BackButton()
        {
            BackBtn.Click();
        }

        public void ExplicitWait(IWebDriver driver, IWebElement BatchOverviewPage)
        {
        
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementExists((By.XPath("//span[contains(text(),'Batch Overview')]"))));
        }

        public void Xyz()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementExists((By.XPath("//span[contains(text(),'Batch Overview')]"))));
        }

        public void MainMenuButton()
        {
            MainMenuBtn.Click();
        }


    }
}
