﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;
using BlommerFramework;
using BlommerFramework.TestData;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;


namespace BlommerFramework
{
    public class BatchProcessPage
    {
        IWebDriver driver;
        public static IWebDriver webDriver;

        [FindsBy(How = How.Id, Using = "btnPasteMixing")]
        public IWebElement PastMixing;
        [FindsBy(How = How.XPath, Using = "//span[contains(text(),'Paste Mixing: Batch')]")]
        public IWebElement PasteMixingBatchPage;
        [FindsBy(How = How.Id, Using = "UCPasteMixNavButtons1_Button4Select")]
        public IWebElement IngredientsTab;
        [FindsBy(How = How.Id, Using = "btnRefining")]
        public IWebElement RefiningTab;
        [FindsBy(How = How.Id, Using = "btnStabilizing")]
        public IWebElement StabilizingTab;
        [FindsBy(How = How.XPath, Using = "//input[@id='Create']")]
        public IWebElement CreateBatchBtn;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_New")]
        public IWebElement NewBtn;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_Back")]
        public IWebElement BackBtn;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_Insert")]
        public IWebElement CreateBtn;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_CreateCancel")]
        public IWebElement CancelBtn;
        [FindsBy(How = How.Id, Using = "GridView_btnSelect_0")]
        public IWebElement SelectBtn;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_NextNum")]
        public IWebElement NextBtn;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_Edit")]
        public IWebElement EditBtn;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_Delete")]
        public IWebElement DeleteBtn;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_Update")]
        public IWebElement SaveBtn;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_SaveCancel")]
        public IWebElement UpdateCancelBtn;
        [FindsBy(How = How.XPath, Using = "//span[contains(text(),'Please provide valid values for the fields marked')]")]
        public IWebElement ValidationMessage;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_BatchID")]
        public IWebElement BatchIDtextBox;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_MainSKU")]
        public IWebElement MainSkutextBox;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_Line")]
        public IWebElement LineDropdown;
        [FindsBy(How = How.Id, Using = "UCPageHeader1_PageBackLink")]
        public IWebElement MainMenuBtn;
        [FindsBy(How = How.Name, Using = "btnOverview")]
        public IWebElement overviewBtn;
        [FindsBy(How = How.XPath, Using = "//td[contains(text(),'Line: *')]")]
        public IWebElement Line;
        [FindsBy(How = How.XPath, Using = "//span[@id='DetailsViewMaintStatus']")]
        public IWebElement RecordsFound;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_Conche")]
        public IWebElement ConcheDropdown;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_FromDate")]
        public IWebElement StartDate;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_ToDate")]
        public IWebElement CompletionDate;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_ProductID")]
        public IWebElement FormulaDropdown;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_SubBatchNum")]
        public IWebElement SubBatchDropdown;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_Quantity")]
        public IWebElement LotWeightTextBox;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_SKU1Qty")]
        public IWebElement SKU1QtyTextBox;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_Status")]
        public IWebElement NotReleased;
        [FindsBy(How = How.Id, Using = "DetailsViewMain_btnAddRework")]
        public IWebElement AddReworkBtn;
        [FindsBy(How = How.Name, Using = "btnClose")]
        public IWebElement closeButton;
        [FindsBy(How = How.Id, Using = "txtLotNumber")]
        public IWebElement LotNumberTextBox;
        [FindsBy(How = How.Id, Using = "btnApplyFilter")]
        public IWebElement applyFilterButton;
        [FindsBy(How = How.Id, Using = "lblLotNumber")]
        public IWebElement searchedLOT;
        [FindsBy(How = How.Name, Using = "btnClearFilter")]
        public IWebElement clearFilterButton;
        [FindsBy(How = How.XPath, Using = "//input[@name='grdRework$ctl02$ctl00']")]
        public IWebElement AddButton;
        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsViewMain_Update']")]
        public IWebElement SaveButton;
        [FindsBy(How = How.XPath, Using = " //span[@id='DetailsViewMain_GVIngredients_lblLocLotNumber_10']")]
        public IWebElement ReworkLOT;
        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsViewMain_GVIngredients_DeleteIngredient_10']")]
        public IWebElement DeleteCheckBox;
        [FindsBy(How = How.XPath, Using = "//span[@id='DetailsViewMain_GVIngredients_IngredientID_10']")]
        public IWebElement AddedSKU;
        [FindsBy(How = How.XPath, Using = "//span[@id='DetailsViewMain_GVIngredients_FormulaQTY_10']")]
        public IWebElement AddedFormula;
        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsViewMain_GVIngredients_FormulaQTY_10']")]
        public IWebElement FormulaIdTextBox;
        [FindsBy(How = How.XPath, Using = "//span[@id='DetailsViewMain_GVIngredients_Label6_10']")]
        public IWebElement AddedSKUafterFormula;

        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsViewMain_GVIngredients_Distribute_10']")]
        public IWebElement DistButton;
        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsViewMain_Quantity']")]
        public IWebElement LotWeight;
        [FindsBy(How = How.Id, Using = "Adjust")]
        public IWebElement AdjustButton;
        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsView_BatchID']")]
        public IWebElement BatchIdTextBox;
        [FindsBy(How = How.XPath, Using = "//span[@id='DetailsView_Status']")]
        public IWebElement Status;
        [FindsBy(How = How.XPath, Using = "//input[@id='Start']")]
        public IWebElement RealeseBtn;
        [FindsBy(How = How.Id, Using = "BatchStatus")]
        public IWebElement BatchListDropdown;
        [FindsBy(How = How.Id, Using = "DetailsView_MixerNo")]
        public IWebElement MixerNoDropdown;
        [FindsBy(How = How.XPath, Using = "//input[@id='btnPasteMixing']")]
        public IWebElement PasteMixingBtn;
        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsView_Back']")]
        public IWebElement PasteMixingStartBtn;
        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsView_Next']")]
        public IWebElement PasteMixingNextBtn;
        [FindsBy(How = How.XPath, Using = "//td[contains(text(),'Lot:')]//following-sibling::td[1]")]
        public IWebElement LotValue;
        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsView_Edit']")]
        public IWebElement EditBtnOnPasteMixing;
        [FindsBy(How = How.Id, Using = "DetailsView_btnUpdate")]
        public IWebElement UpdatebtninPasteMixing;
        [FindsBy(How = How.Id, Using = "DetailsView_btnFinish")]
        public IWebElement FinishbtninPasteMixing;
        [FindsBy(How = How.Id, Using = "DetailsView_btnAddRework")]
        public IWebElement AddReworkbtninPasteMixing;
        [FindsBy(How = How.Id, Using = "DetailsView_btnExtraIngredients")]
        public IWebElement AddIngrebtninPasteMixing;
        [FindsBy(How = How.Id, Using = "DetailsView_GVIngredients_lblLotNumber_9")]
        public IWebElement lotAddedafterRework;
        [FindsBy(How = How.Id, Using = "DetailsView_imgSearch")]
        public IWebElement MagnifyingGlass;
        [FindsBy(How = How.Id, Using = "Close")]
        public IWebElement ClosebtnonAvailableContainer;
        [FindsBy(How = How.Id, Using = "DetailsView_Container")]
        public IWebElement ContainerBox;
        [FindsBy(How = How.Id, Using = "DetailsView_txtLotNumber")]
        public IWebElement LotNUmber;
        [FindsBy(How = How.Id, Using = "DetailsView_txtWeightUsed")]
        public IWebElement PoundWeightBox;
        [FindsBy(How = How.Id, Using = "DetailsView_btnSaveWeightUsed")]
        public IWebElement SaveBtnonPasteMixing;
        [FindsBy(How = How.XPath, Using = "//button[@class='ui-button ui-corner-all ui-widget']")]
        public IWebElement ConfirmationOkBtn;

        [FindsBy(How = How.Id, Using = "DetailsView_Remaining")]
        public IWebElement ActualWeightBefore;
        [FindsBy(How = How.Id, Using = "DetailsView_InfoValue")]
        public IWebElement RemainingQty;
        [FindsBy(How = How.Id, Using = "DetailsView_GVPounds_Delete_0")]
        public IWebElement DeleteIcon;
        [FindsBy(How = How.Id, Using = "DetailsView_UCSubBatchButtons1_Button2Select")]
        public IWebElement Button2;
        [FindsBy(How = How.Id, Using = "UCPasteMixNavButtons1_Button6Select")]
        public IWebElement SummaryButton;
        [FindsBy(How = How.Id, Using = "DetailsView_Next")]
        public IWebElement FinishbtninPasteMixingSummaryPage;
        [FindsBy(How = How.XPath, Using = "//span[@id='DetailsViewMaintStatus']")]
        public IWebElement NoRecordsfound;
        [FindsBy(How = How.XPath, Using = "//input[@id='btnRefining']")]
        public IWebElement RefiningBtn;
        [FindsBy(How = How.Id, Using = "UCRefiningNavButtons1_Button6Select")]
        public IWebElement SummaryButtonofRefining;
        [FindsBy(How = How.XPath, Using = "//input[@id='Create']")]
        public IWebElement CreateBatch;
        [FindsBy(How = How.Name, Using = "btnOverview")]
        public IWebElement OVERBtn;

        public bool MainMenuBatchProcessPage()
        {
            return Browser.Title.Contains("Main Menu - Batch Process");
        }
        public bool PasteMixingInstructionPage()
        {
            return Browser.Title.Contains("Paste Mixing: Instructions - Batch Process");
        }
        public bool PasteMixingIngredientsPage()
        {
            return Browser.Title.Contains("Paste Mixing: Ingredients - Batch Process");
        }
        public bool PasteMixingSummaryPage()
        {
            return Browser.Title.Contains("Paste Mixing Summary - Batch Process");
        }
        public bool RefinningSummaryPage()
        {
            return Browser.Title.Contains("Refining: Summary - Batch Process");
        }
        public bool AvailableContainersPage()
        {
            return Browser.Title.Contains("Available Containers - Batch Process");
        }
        public bool RefiningBatchPage()
        {
            return Browser.Title.Contains("Refinning - Batch Process");
        }
        public bool RefiningInstructionPage()
        {
            return Browser.Title.Contains("Refinning: Instructions - Batch Process");
        }

        public bool StabilizingBatchPage()
        {
            return Browser.Title.Contains("Stabilizing - Batch Process");
        }
        public bool PasteMixingBatch()
        {
            return Browser.Title.Contains("Paste Mixing - Batch Process");
        }
        public void PasteMixingTab()
        {
            PastMixing.Click();
        }
        public void Ingredients()
        {
            IngredientsTab.Click();
        }
        public void Refining()
        {
            RefiningTab.Click();
        }
        public void Stabilizing()
        {
            StabilizingTab.Click();
        }

        public bool StabilizingBatch()
        {
            return Browser.Title.Contains("Stabilizing - Batch Process");
        }
        public bool RefiningBatch()
        {
            return Browser.Title.Contains("Refinning - Batch Process");
        }


        public void AvailableButtonwithandWithoutExistingBatch()
        {
            CreateBatchBtn.Click();

            bool New = Pages.BatchProcess.NewBtn.Displayed;
            Assert.IsTrue(Pages.BatchProcess.NewBtn.Displayed, "New Button Displayed is"+ New);
            bool Back = Pages.BatchProcess.BackBtn.Displayed;
            Assert.IsTrue(Pages.BatchProcess.BackBtn.Displayed, "Back Button Displayed is"+Back);
            NewBtn.Click();
            Browser.ExplicityWait(CreateBtn);
            bool Create = Pages.BatchProcess.CreateBtn.Displayed;
            Assert.IsTrue(Pages.BatchProcess.CreateBtn.Displayed, "Create Button Displayed is" + Create);
            bool Cancel = Pages.BatchProcess.CancelBtn.Displayed;
            Assert.IsTrue(Pages.BatchProcess.CancelBtn.Displayed, "Cancel Button Displayed is" + Cancel);
            CancelBtn.Click();
            Browser.ExplicityWait(BackBtn);
            BackBtn.Click();
            Browser.ExplicityWait(SelectBtn);
            SelectBtn.Click();
            Browser.ExplicityWait(CreateBatchBtn);
            CreateBatchBtn.Click();
            Browser.ExplicityWait(EditBtn);
            bool Edit = Pages.BatchProcess.EditBtn.Displayed;
            Assert.IsTrue(Pages.BatchProcess.EditBtn.Displayed, "Edit Button for selected Batch displayed is:" + Edit);
            bool NEW = Pages.BatchProcess.NewBtn.Displayed;
            Assert.IsTrue(Pages.BatchProcess.NewBtn.Displayed, "New Button for selected Batch displayed is:"+ NEW);
            bool Delete = Pages.BatchProcess.DeleteBtn.Displayed;
            Assert.IsTrue(Pages.BatchProcess.DeleteBtn.Displayed, "Delete Button for selected Batch displayed is:" + Delete);
            EditBtn.Click();
            Browser.ExplicityWait(SaveBtn);
            bool Save = Pages.BatchProcess.SaveBtn.Displayed;
            Assert.IsTrue(Pages.BatchProcess.SaveBtn.Displayed, "Save Button for selected Batch displayed is:" + Save);
            bool Update = Pages.BatchProcess.UpdateCancelBtn.Displayed;
            Assert.IsTrue(Pages.BatchProcess.UpdateCancelBtn.Displayed, "Cancel Button for selected Batch displayed is:" + Update);
            UpdateCancelBtn.Click();
            Browser.ExplicityWait(BackBtn);
            BackBtn.Click();
            Browser.ExplicityWait(MainMenuBtn);
            MainMenuBtn.Click();
            Browser.ExplicityWait(overviewBtn);
            overviewBtn.Click();
        }

        public void CreateNewBatch(string testName)
        {
            Browser.ExplicityWait(CreateBatchBtn);
            CreateBatchBtn.Click();
            Browser.ExplicityWait(NewBtn);
            NewBtn.Click();
            Browser.ExplicityWait(CreateBtn);
            CreateBtn.Click();
            Browser.ExplicityWait(ValidationMessage);
            bool ValidationMsg = Pages.BatchProcess.ValidationMessage.Displayed;
            Assert.IsTrue(Pages.BatchProcess.ValidationMessage.Displayed, "Validation Message displayed is:"+ ValidationMsg);
            Browser.ExplicityWait(CancelBtn);
            CancelBtn.Click();
            Browser.ExplicityWait(NewBtn);
            NewBtn.Click();
            Browser.ExplicityWait(CreateBtn);
            bool CreateButton = Pages.BatchProcess.CreateBtn.Displayed;
            Assert.IsTrue(Pages.BatchProcess.CreateBtn.Displayed, "Create Button Displayed is:"+ CreateButton);
            var UserData = DataAccess.GetTestData(testName);
            BatchIDtextBox.SendKeys(UserData.BatchID);
            Line.Click();
            System.Threading.Thread.Sleep(5000);
            Browser.ExplicityWait(RecordsFound);
            // System.Threading.Thread.Sleep(5000);
            bool Records = Pages.BatchProcess.RecordsFound.Displayed;
            Assert.IsTrue(Pages.BatchProcess.RecordsFound.Displayed, "New Record Created is: "+ Records);
            MainSkutextBox.SendKeys(UserData.SKU);
            Line.Click();
             System.Threading.Thread.Sleep(5000);
            Browser.ExplicityWait(LineDropdown);
            SelectElement oSelect = new SelectElement(LineDropdown);
            oSelect.SelectByIndex(1);
            System.Threading.Thread.Sleep(5000);
            Browser.ExplicityWait(ConcheDropdown);
            SelectElement oSelect1 = new SelectElement(ConcheDropdown);
            oSelect1.SelectByIndex(1);

           string Startdate= DateTime.Now.ToString("MM/dd/yyyy");

            string Completiondate = DateTime.Now.AddDays(1).ToString("MM/dd/yyyy");


            StartDate.SendKeys(Startdate);
            // StartDate.SendKeys(UserData.StartDate);
            CompletionDate.SendKeys(Completiondate);
         //   CompletionDate.SendKeys(UserData.CompletionDate);
            SelectElement oSelect2 = new SelectElement(FormulaDropdown);
            oSelect2.SelectByIndex(1);

              System.Threading.Thread.Sleep(5000);
            Browser.ExplicityWait(SubBatchDropdown);
            SelectElement oSelect3 = new SelectElement(SubBatchDropdown);
            oSelect3.SelectByIndex(2);
            System.Threading.Thread.Sleep(10000);
          
            SKU1QtyTextBox.SendKeys(UserData.SkuWeight);
            Line.Click();
            System.Threading.Thread.Sleep(5000);
            Browser.ExplicityWait(CreateBtn);
            CreateBtn.Click();
            System.Threading.Thread.Sleep(5000);
            Browser.ExplicityWait(NotReleased);
            bool NotRelease = Pages.BatchProcess.NotReleased.Displayed;
            Assert.IsTrue(Pages.BatchProcess.NotReleased.Displayed, "New Batch status Not Released is: "+ NotRelease);

        }

        public void AddandRemoveRework(string testName)
        {
            EditBtn.Click();
            Browser.ExplicityWait(AddReworkBtn);
            bool AddRework = Pages.BatchProcess.AddReworkBtn.Displayed;
            Assert.IsTrue(Pages.BatchProcess.AddReworkBtn.Displayed, "Add Rework button displayed is: "+ AddRework);
            AddReworkBtn.Click();
            Browser.ExplicityWait(closeButton);
           System.Threading.Thread.Sleep(5000);
            Assert.IsTrue(Pages.Rework.IsAt());
            closeButton.Click();
            Browser.ExplicityWait(AddReworkBtn);
         
            Assert.IsTrue(Pages.Overview.BatchMaintanancePage());
            AddReworkBtn.Click();
            Browser.ExplicityWait(LotNumberTextBox);
       
            Assert.IsTrue(Pages.Rework.IsAt());
            var UserData = DataAccess.GetTestData(testName);
            LotNumberTextBox.SendKeys(UserData.LOT);
            Browser.ExplicityWait(applyFilterButton);
            System.Threading.Thread.Sleep(5000);
            applyFilterButton.Click();
        
            Browser.ExplicityWait(AddButton);
            string ActualLot = UserData.LOT;
            string SearchedLot = searchedLOT.Text;
            Assert.AreEqual(ActualLot, SearchedLot, "LOT value is displayed as:"+ SearchedLot);
            
            AddButton.Click();
            Browser.ExplicityWait(AddedSKU);

            bool SKU = Pages.BatchProcess.AddedSKU.Displayed;
            Assert.IsTrue(Pages.BatchProcess.AddedSKU.Displayed, "Rework added is:" + SKU);

            string ActualLOT = ReworkLOT.Text;
            string ExpectedLOT = "C175008";
            Assert.AreEqual(ActualLOT, ExpectedLOT, "Value Display in LOC:"+ ActualLOT);
            
            SaveButton.Click();
           
            Browser.alert();
            Browser.ExplicityWait(EditBtn);
         

            EditBtn.Click();
            Browser.ExplicityWait(DeleteCheckBox);
            DeleteCheckBox.Click();
            Browser.ExplicityWait(SaveButton);
            SaveButton.Click();
            
            Browser.alert();
          
        }

        public void DistributeReworkIngredients(string testName)
        {
            Browser.ExplicityWait(EditBtn);
            EditBtn.Click();
          
            Browser.ExplicityWait(AddReworkBtn);
            bool AddRework = Pages.BatchProcess.AddReworkBtn.Displayed;
            Assert.IsTrue(Pages.BatchProcess.AddReworkBtn.Displayed, "Add Rework button displayed is:"+ AddRework);
            AddReworkBtn.Click();
            Browser.ExplicityWait(LotNumberTextBox);
            
            Assert.IsTrue(Pages.Rework.IsAt());
            var UserData = DataAccess.GetTestData(testName);
            LotNumberTextBox.SendKeys(UserData.LOT);
            Browser.ExplicityWait(applyFilterButton);
            System.Threading.Thread.Sleep(5000);
            applyFilterButton.Click();
            Browser.ExplicityWait(AddButton);
            
            AddButton.Click();
            Browser.ExplicityWait(AddedSKU);
            bool SKU = Pages.BatchProcess.AddedSKU.Displayed;
            Assert.IsTrue(Pages.BatchProcess.AddedSKU.Displayed, "Rework added is:"+SKU);

            string PreviousLotWeight1 = LotWeight.GetAttribute("value");
            
            double PrevWeight = Convert.ToDouble(PreviousLotWeight1);
            

            FormulaIdTextBox.Clear();
            Browser.ExplicityWait(FormulaIdTextBox);
          
            FormulaIdTextBox.SendKeys(UserData.FormulaIDValue);
            Browser.ExplicityWait(DistButton);
           

            DistButton.Click();
            System.Threading.Thread.Sleep(20000);
                                
            if (IsElementPresent(AdjustButton)==true)
            {
            }
            else
            { 
                DistButton.Click();
                System.Threading.Thread.Sleep(20000);
            }

            bool Adjust = Pages.BatchProcess.AdjustButton.Displayed;
            Assert.IsTrue(Pages.BatchProcess.AdjustButton.Displayed, "Adjust button display for distrubution is:"+ Adjust);

            double UpdatedLotWeight = PrevWeight + 1000.00;
          
            string AfterLotWeight1 = LotWeight.GetAttribute("value");

            double AfterWeight = Convert.ToDouble(AfterLotWeight1);
            Assert.AreEqual(UpdatedLotWeight, AfterWeight, "Lot Weight is Increase");
          
            AdjustButton.Click();
            Browser.ExplicityWait(SaveButton);
        
            SaveButton.Click();
           
            Browser.alert();
            Browser.ExplicityWait(AddedSKUafterFormula);

            bool SKUAFTERFORMULA = Pages.BatchProcess.AddedSKUafterFormula.Displayed;
            Assert.IsTrue(Pages.BatchProcess.AddedSKUafterFormula.Displayed, "Rework added is:"+ SKUAFTERFORMULA);
            bool AddedFormul = Pages.BatchProcess.AddedFormula.Displayed;
            Assert.IsTrue(Pages.BatchProcess.AddedFormula.Displayed, "Rework added formula displayed is:"+ AddedFormul);
           
        }

        public bool IsElementPresent(IWebElement element)
        {
            try
            {
              bool a =  element.Displayed;
              return true;
           
            }
            catch
            {
                return false;
            }
            
        }

        public void BatchReleased(string testName)
        {
            Browser.ExplicityWait(BackBtn);
            BackBtn.Click();
      

            Browser.ExplicityWait(Status);
            Assert.IsTrue(Pages.Overview.IsAt());
           
            var UserData = DataAccess.GetTestData(testName);
          
            string ActualStatus = Status.Text;
            string ExpectedStatus = "Not Released";
            Assert.AreEqual(ExpectedStatus, ActualStatus, " Batch Status is displayed as " + ActualStatus);
            System.Threading.Thread.Sleep(5000);
            RealeseBtn.Click();
            Browser.ExplicityWait(BatchIdTextBox);
            System.Threading.Thread.Sleep(5000);

            string ActualValue = BatchIdTextBox.Text;
            string ExpectedValue = "";
            Assert.AreEqual(ActualValue, ExpectedValue, " Batch record removed from New Batch List  " + ActualValue);

            SelectElement oSelect = new SelectElement(BatchListDropdown);
            oSelect.SelectByIndex(1);
          
            BatchIdTextBox.SendKeys(UserData.BatchID);
            BatchIdTextBox.SendKeys(Keys.Enter);

            Browser.ExplicityWait(Status);
            string ActualStatus1 = Status.Text;
            string ExpectedStatus1 = "Not Started";
            Assert.AreEqual(ActualStatus1, ExpectedStatus1, " Batch is Displayed is InProgress batch list with status: " + ActualStatus1);
           
        }

        public void AddandRemoveReworkinPasteMixing(string testName)
        {
            Browser.ExplicityWait(MainMenuBtn);
            MainMenuBtn.Click();
            Browser.ExplicityWait(PasteMixingBtn);
            Assert.IsTrue(Pages.BatchProcess.MainMenuBatchProcessPage());
            PasteMixingBtn.Click();
           
            Browser.ExplicityWait(BatchIdTextBox);
            Assert.IsTrue(Pages.BatchProcess.PasteMixingBatch());

            var UserData = DataAccess.GetTestData(testName);
            BatchIdTextBox.SendKeys(UserData.BatchID);
            BatchIdTextBox.SendKeys(Keys.Enter);
            Browser.ExplicityWait(PasteMixingStartBtn);

            PasteMixingStartBtn.Click();
            Browser.ExplicityWait(PasteMixingNextBtn);

            Assert.IsTrue(Pages.BatchProcess.PasteMixingInstructionPage());
           
            PasteMixingNextBtn.Click();
          
            Browser.ExplicityWait(LotValue);
            Assert.IsTrue(Pages.BatchProcess.PasteMixingIngredientsPage());

            string ActualLotValue = LotValue.Text;
            string ExpectedLotValue = "C175008";
            Assert.AreEqual(ActualLotValue, ExpectedLotValue, "Lot values shows for rework row is" + ActualLotValue);
            Browser.ExplicityWait(EditBtnOnPasteMixing);
            EditBtnOnPasteMixing.Click();
           
            Browser.ExplicityWait(UpdatebtninPasteMixing);
            bool Update = Pages.BatchProcess.UpdatebtninPasteMixing.Displayed;
            bool Finish = Pages.BatchProcess.FinishbtninPasteMixing.Displayed;
            bool addrework = Pages.BatchProcess.AddReworkbtninPasteMixing.Displayed;
            bool AddIngred = Pages.BatchProcess.AddIngrebtninPasteMixing.Displayed;

            Assert.IsTrue(Pages.BatchProcess.UpdatebtninPasteMixing.Displayed, "Update Button display is:"+ Update);
            Assert.IsTrue(Pages.BatchProcess.FinishbtninPasteMixing.Displayed, "Finish Button display is:"+ Finish);
            Assert.IsTrue(Pages.BatchProcess.AddReworkbtninPasteMixing.Displayed, "Add Rework Button display is:"+ addrework);
            Assert.IsTrue(Pages.BatchProcess.AddIngrebtninPasteMixing.Displayed, "Add Ingredients Button display is:"+ AddIngred);
            AddReworkbtninPasteMixing.Click();
          
            Browser.ExplicityWait(LotNumberTextBox);
            Assert.IsTrue(Pages.Rework.IsAt());


            LotNumberTextBox.SendKeys(UserData.LOT);
            Browser.ExplicityWait(applyFilterButton);
            System.Threading.Thread.Sleep(5000);
            applyFilterButton.Click();
          
            Browser.ExplicityWait(searchedLOT);
            string ActualLot = UserData.LOT;
           
            string SearchedLot = searchedLOT.Text;
            Assert.AreEqual(ActualLot, SearchedLot, "LOT value is displayed as:"+ SearchedLot);
           
            AddButton.Click();
            Browser.ExplicityWait(lotAddedafterRework);
          
            Assert.IsTrue(Pages.BatchProcess.PasteMixingIngredientsPage());
            bool lot = Pages.BatchProcess.lotAddedafterRework.Displayed;
            Assert.IsTrue(Pages.BatchProcess.lotAddedafterRework.Displayed, "Added rework display with Lot is: "+lot);
            string ActualLotValue1 = lotAddedafterRework.Text;
            string ExpectedLotValue1 = "C175008";
            Assert.AreEqual(ExpectedLotValue1, ActualLotValue1, "After Add Rework Lot values shows for rework row is" + ActualLotValue);

        }

        public void AddandDeleteActualQuantitiesUsedinPasteMixing(string testName)
        {
            Browser.ExplicityWait(MixerNoDropdown);
            Assert.IsTrue(Pages.BatchProcess.PasteMixingIngredientsPage());
            SelectElement oSelect = new SelectElement(MixerNoDropdown);
            oSelect.SelectByIndex(1);
            MagnifyingGlass.Click();
          
            Browser.ExplicityWait(ClosebtnonAvailableContainer);
            Assert.IsTrue(Pages.BatchProcess.AvailableContainersPage());
            ClosebtnonAvailableContainer.Click();
          
            Browser.ExplicityWait(ContainerBox);
            Assert.IsTrue(Pages.BatchProcess.PasteMixingIngredientsPage());

            var UserData = DataAccess.GetTestData(testName);
            ContainerBox.SendKeys(UserData.Container);
            ContainerBox.SendKeys(Keys.Enter);
            System.Threading.Thread.Sleep(10000);

            string ActualLotValue1 = LotNUmber.GetAttribute("value");
            string ExpectedLotValue1 = "C175008";
            Assert.AreEqual(ExpectedLotValue1, ActualLotValue1, "Lot value auto generated based on Container value" + ActualLotValue1);
            
          
            Browser.ExplicityWait(ActualWeightBefore);

            string Amount = ActualWeightBefore.Text;
            double PrevWeight = Convert.ToDouble(Amount);

            PoundWeightBox.SendKeys(UserData.Location);
           

            SaveBtnonPasteMixing.Click();
            System.Threading.Thread.Sleep(3000);
            Browser.ExplicityWait(ConfirmationOkBtn);
         System.Threading.Thread.Sleep(6000);
            ConfirmationOkBtn.Click();
          System.Threading.Thread.Sleep(3000);

            double UpdatedActualWeight = PrevWeight - 1.00;
           
            string AfterActualWeight1 = ActualWeightBefore.Text;

            double AfterWeight = Convert.ToDouble(AfterActualWeight1);
            ContainerBox.SendKeys(UserData.Container);
            ContainerBox.SendKeys(Keys.Enter);
          System.Threading.Thread.Sleep(5000);
           

            string remainingQty= RemainingQty.Text;
            string ExpectedQty = "11";
            

            DeleteIcon.Click();
            Browser.alert();
             System.Threading.Thread.Sleep(15000);

            ContainerBox.Clear();
            ContainerBox.SendKeys(UserData.Container);
            ContainerBox.SendKeys(Keys.Enter);

         System.Threading.Thread.Sleep(8000);


            string remainingQty1 = RemainingQty.Text;
            string ExpectedQty1 = "12";
            string ActualWeight = ActualWeightBefore.Text;
            string ExpectedActualWeight = "500.00";
           
            Browser.ExplicityWait(FinishbtninPasteMixing);
            FinishbtninPasteMixing.Click();
           System.Threading.Thread.Sleep(8000);
            Browser.ExplicityWait(Button2);
            Button2.Click();
           System.Threading.Thread.Sleep(5000);
            Browser.ExplicityWait(EditBtnOnPasteMixing);
            EditBtnOnPasteMixing.Click();
            System.Threading.Thread.Sleep(8000);
            SelectElement oSelect1 = new SelectElement(MixerNoDropdown);
            oSelect1.SelectByIndex(1);
            Browser.ExplicityWait(FinishbtninPasteMixing);
            FinishbtninPasteMixing.Click();
            Browser.ExplicityWait(SummaryButton);
           System.Threading.Thread.Sleep(5000);
            SummaryButton.Click();
            Browser.ExplicityWait(FinishbtninPasteMixingSummaryPage);
          System.Threading.Thread.Sleep(5000);

            Assert.IsTrue(Pages.BatchProcess.PasteMixingSummaryPage());
            FinishbtninPasteMixingSummaryPage.Click();
            Browser.alert();
            System.Threading.Thread.Sleep(5000);
            

        }

        public void Refining(string testName)
        {
            MainMenuBtn.Click();
            Browser.ExplicityWait(RefiningBtn);
            Assert.IsTrue(Pages.BatchProcess.MainMenuBatchProcessPage());
            RefiningBtn.Click();
            Browser.ExplicityWait(BatchIdTextBox);
          
            Assert.IsTrue(Pages.BatchProcess.RefiningBatch());

            var UserData = DataAccess.GetTestData(testName);
            BatchIdTextBox.SendKeys(UserData.BatchID);
            BatchIdTextBox.SendKeys(Keys.Enter);

            Browser.ExplicityWait(Status);

            string ActualStatus1 = Status.Text;
            string ExpectedStatus1 = "Ready for Refining";
            Assert.AreEqual(ActualStatus1, ExpectedStatus1, "Batch is Displayed in Refining with status: " + ActualStatus1);
            Browser.ExplicityWait(PasteMixingStartBtn);
            PasteMixingStartBtn.Click();
          
            Browser.ExplicityWait(SummaryButtonofRefining);
            Assert.IsTrue(Pages.BatchProcess.RefiningInstructionPage());
           

            SummaryButtonofRefining.Click();
            System.Threading.Thread.Sleep(5000);

            Browser.ExplicityWait(FinishbtninPasteMixingSummaryPage);
            Assert.IsTrue(Pages.BatchProcess.RefinningSummaryPage());
            FinishbtninPasteMixingSummaryPage.Click();
            Browser.alert();
            System.Threading.Thread.Sleep(8000);
          
        }

        public void DeleteBatch(string testName)
        {
            MainMenuBtn.Click();
            Browser.ExplicityWait(OVERBtn);
            Assert.IsTrue(Pages.BatchProcess.MainMenuBatchProcessPage());
         
            Pages.TopNavigation.ClickOverviewBtn();
            Assert.IsTrue(Pages.Overview.IsAt());
            Browser.ExplicityWait(BatchListDropdown);

            SelectElement oSelect = new SelectElement(BatchListDropdown);
            oSelect.SelectByIndex(1);
            Browser.ExplicityWait(BatchIdTextBox);
            var UserData = DataAccess.GetTestData(testName);
            BatchIdTextBox.SendKeys(UserData.BatchID);
            BatchIdTextBox.SendKeys(Keys.Enter);
            Browser.ExplicityWait(Status);
           
            string ActualStatus1 = Status.Text;
            string ExpectedStatus1 = "Ready for Stabilizing";
            Assert.AreEqual(ExpectedStatus1, ActualStatus1, "Batch is Displayed is InProgress batch list with status: " + ActualStatus1);
           
            Pages.Rework.UnReleaseBatch("BatchProcess");
           

           System.Threading.Thread.Sleep(3000);
            Browser.ExplicityWait(CreateBatch);
            CreateBatch.Click();
            System.Threading.Thread.Sleep(3000);
            Browser.ExplicityWait(DeleteBtn);

            Assert.IsTrue(Pages.Overview.BatchMaintanancePage());
            DeleteBtn.Click();
            
            Browser.alert();
          
            Browser.ExplicityWait(NewBtn);
            bool newBTn= Pages.BatchProcess.NewBtn.Displayed;
            bool bckbtn = Pages.BatchProcess.NewBtn.Displayed;

            Assert.IsTrue(Pages.BatchProcess.NewBtn.Displayed, "New Button Displayed is: "+ newBTn);
            Assert.IsTrue(Pages.BatchProcess.BackBtn.Displayed, "Back Button Displayed is: "+ bckbtn);
            BackBtn.Click();
            System.Threading.Thread.Sleep(5000);

            Assert.IsTrue(Pages.Overview.IsAt());
           
            Pages.Login.Logout();
           

        }
    }
   }