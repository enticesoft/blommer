using BlommerFramework.TestData;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using BlommerFramework.TestData;
using NUnit.Framework;
using System;

namespace BlommerFramework
{
    public class LoginPage
    {
        [FindsBy(How = How.Id, Using = "UserName")]
        private IWebElement userName;

        [FindsBy(How = How.Id, Using = "Password")]
        private IWebElement passwordField;

        [FindsBy(How = How.Name, Using = "ctl08")]
        private IWebElement loginButton;

        [FindsBy(How = How.Id, Using = "lblErrorMessage")]
        public IWebElement validationMsg;

        [FindsBy(How = How.Id, Using = "UCPageHeader1_LogOut")]
        public IWebElement logoutButton;

        [FindsBy(How = How.Id, Using = "btnBatchStart")]
        private IWebElement StartBtn;
        [FindsBy(How = How.Id, Using = "btnBatchCleanUp")]
        private IWebElement CleanUpBtn;
        [FindsBy(How = How.Id, Using = "lblStartResult")]
        private IWebElement StartBtnResult;
        [FindsBy(How = How.XPath, Using = "//label[@id='lblCleanUpResult'][contains(text(),'OK')]")]
        private IWebElement CleanUpBtnResult;

        public bool IsAt()
        {
            return Browser.Title.Contains("Login - Batch Process");
        }

        public bool TestInitializerpage()
        {
            return Browser.Title.Contains("Tests Initializer");
        }
        public bool TestInitializerpage()
        {
            return Browser.Title.Contains("Tests Initializer");

        }

        private void ClearLoginFields()
        {
            userName.Clear();
            passwordField.Clear();
        }

        public void LogIn(string testName)
        {       

            var UserData = DataAccess.GetTestData(testName);
            userName.Clear();
            userName.SendKeys(UserData.Username);
            passwordField.SendKeys(UserData.Password);
            loginButton.Click();
        }

        public void InValidLogIn(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            ClearLoginFields();
            userName.SendKeys(UserData.InvalidUsername);
            passwordField.SendKeys(UserData.InvalidPassword);
            loginButton.Click();
        }

        public void Logout()
        {
            logoutButton.Click();
        }

        public bool InitializerTest()
        {
             Assert.IsTrue(Pages.Login.StartBtn.Displayed);
            Assert.IsTrue(Pages.Login.CleanUpBtn.Displayed);
            StartBtn.Click();
            System.Threading.Thread.Sleep(5000);
            Browser.ExplicityWait(StartBtnResult);

            var isOK = Equals("OK", StartBtnResult.Text);
            Assert.IsTrue(isOK, "Batch Process Initialization failed!");

            return isOK;
        }

        public void GotoLoginPage()
        {
           
            Browser.Goto("");
        }


        public bool InitializerTest()
        {

            Assert.IsTrue(Pages.Login.StartBtn.Displayed);
            Assert.IsTrue(Pages.Login.CleanUpBtn.Displayed);
            StartBtn.Click();
            System.Threading.Thread.Sleep(5000);
            Browser.ExplicityWait(StartBtnResult);
        
            string Result = StartBtnResult.Text;
            Console.WriteLine("xyz" + Result);

            if (Equals(Result, "OK")){
                return true;
            }else
            {
                return false;
            }           
            
        }

        public void CleanUpTest()
        {

            Assert.IsTrue(Pages.Login.StartBtn.Displayed);
            Assert.IsTrue(Pages.Login.CleanUpBtn.Displayed);
            CleanUpBtn.Click();
            Browser.ExplicityWait(CleanUpBtnResult);
            string Result = CleanUpBtnResult.Text;
            Assert.AreEqual("OK", Result, "Batch Process Clean Up correctly");


        }
    }
}