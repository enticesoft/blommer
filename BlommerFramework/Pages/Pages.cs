﻿using BlommerFramework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace BlommerFramework
{
    public static class Pages
    {

       // public static IWebDriver _driver;

        private static T GetPage<T>() where T : new()
        {
            var page = new T();
            IWebDriver driver = Browser._Driver;
            PageFactory.InitElements(Browser.Driver, page);
            
            return page;
        }

        public static LoginPage Login
        {
            get { return GetPage<LoginPage>(); }
        }

        public static TaskListPage TaskList
        {
            get { return GetPage<TaskListPage>(); }
        }

        public static OverviewPage Overview
        {
            get { return GetPage<OverviewPage>(); }
        }

        public static ReworkPage Rework
        {
            get { return GetPage<ReworkPage>(); }
        }

        public static NavigationOptions TopNavigation
        {
            get { return GetPage<NavigationOptions>(); }
        }

        public static BatchProcessPage BatchProcess
        {
            get { return GetPage<BatchProcessPage>(); }
        }
    }
}
