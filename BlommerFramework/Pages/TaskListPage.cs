﻿using BlommerFramework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace BlommerFramework
{
    public class TaskListPage
    {

        public void Goto()
        {
            Pages.Login.LogIn("login");
            Browser.Goto("BatchSystem/TaskList.aspx");            
        }

        public bool IsAt()
        {
            return Browser.Title.Contains("Main Menu - Batch Process");
        }


    }
}
