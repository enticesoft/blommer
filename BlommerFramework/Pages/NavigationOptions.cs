﻿using BlommerFramework.Generators;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace BlommerFramework
{
    public class NavigationOptions
    {
        [FindsBy(How = How.Name, Using = "btnOverview")]
        public IWebElement overviewBtn;

        [FindsBy(How = How.Name, Using = "btnRework")]
        public IWebElement reworkBtn;

        public void ClickOverviewBtn()
        {
            overviewBtn.Click();
        }

        public void ClickReworkBtn()
        {
            reworkBtn.Click();
        }
    }
}