﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;
using BlommerFramework;
using BlommerFramework.TestData;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;


namespace BlommerFramework
{
    public class ReworkPage
    {
        IWebDriver driver;

        [FindsBy(How = How.XPath, Using = "//table[@class='table']")]
        public IWebElement datatable;

        [FindsBy(How = How.Id, Using = "btnApplyFilter")]
        public IWebElement applyFilterButton;

        [FindsBy(How = How.Name, Using = "btnClearFilter")]
        public IWebElement clearFilterButton;

        [FindsBy(How = How.Name, Using = "btnClose")]
        public IWebElement closeButton;

        [FindsBy(How = How.XPath, Using = "//tr[@class='bccbp-pager']")]
        public IWebElement paginationSec;

        [FindsBy(How = How.Id, Using = "txtSku")]
        public IWebElement SKUTextBox;

        [FindsBy(How = How.Id, Using = "txtDescription")]
        public IWebElement DescriptionTextBox;

        [FindsBy(How = How.Id, Using = "txtLotNumber")]
        public IWebElement LotNumberTextBox;

        [FindsBy(How = How.Id, Using = "txtFormulaID")]
        public IWebElement FormulaIDTextBox;

        [FindsBy(How = How.Id, Using = "txtBuildingID")]
        public IWebElement BuildingIDTextBox;

        [FindsBy(How = How.Id, Using = "txtLocationID")]
        public IWebElement LocationTextBox;
        [FindsBy(How = How.Id, Using = "txtContainerID")]
        public IWebElement ContainerTextBox;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'Plant:')]")]
        public IWebElement PlantLabel;
        [FindsBy(How = How.Id, Using = "lblPlantID")]
        public IWebElement PlantValue;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'Formula ID:')]")]
        public IWebElement FormulaIDLabel;
        [FindsBy(How = How.Id, Using = "lblFormulaID")]
        public IWebElement FormulaIDValue;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'Total Rows:')]")]
        public IWebElement TotalRowsLabel;
        [FindsBy(How = How.Id, Using = "lblTotalRows")]
        public IWebElement TotalRowsValue;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'Total Weight:')]")]
        public IWebElement TotalWeightsLabel;
        [FindsBy(How = How.Id, Using = "lblTotalWeight")]
        public IWebElement TotalWeightsValue;

        [FindsBy(How = How.XPath, Using = "//table[@id='grdRework']//tbody//table//td[2]//a")]
        public IWebElement SecondPaginationButton;
        [FindsBy(How = How.XPath, Using = "//table[@id='grdRework']//tbody//table//td[1]//a")]
        public IWebElement FirstPaginationButton;

        [FindsBy(How = How.Id, Using = "lblSKU")]
        public IWebElement searchedSKU;
        [FindsBy(How = How.Id, Using = "lblDescription")]
        public IWebElement searchedDescription;
        [FindsBy(How = How.XPath, Using = "//table[@id='grdRework']//tbody//tr[2]//td[4]//span")]
        public IWebElement searchedFormulaID;
        [FindsBy(How = How.XPath, Using = "//table[@id='grdRework']//tbody//tr[2]//td[5]//span")]
        public IWebElement searchedBuilding;
        [FindsBy(How = How.XPath, Using = "//table[@id='grdRework']//tbody//tr[2]//td[6]//span")]
        public IWebElement searchedLocation;
        [FindsBy(How = How.Id, Using = "lblLotNumber")]
        public IWebElement searchedLOT;
        [FindsBy(How = How.XPath, Using = "//table[@id='grdRework']//tbody//tr[2]//td[8]//span")]
        public IWebElement searchedContainer;

        [FindsBy(How = How.XPath, Using = "//td[contains(text(),'No records found.')]")]
        public IWebElement NoRecordFound;

        [FindsBy(How = How.XPath, Using = "//span[@id='lblTotalWeight']")]
        public IWebElement TotalWt;

        [FindsBy(How = How.XPath, Using = "//table[@id='grdRework']//tbody//tr[2]//td[9]")]
        public IWebElement ActualWt;

        [FindsBy(How = How.Id, Using = "btnRework")]
        public IWebElement ReworkBtnwt;
        [FindsBy(How = How.XPath, Using = "//td[2]//a[1]")]
        public IWebElement PaginationBtn;


        [FindsBy(How = How.XPath, Using = "(//tr[@class='bccbp-pager']//a)[1]")]
        public IWebElement PaginationBtn1;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(),'IOD-SS3%')]")]
        public IWebElement ClearFilterFormulaId;


        [FindsBy(How = How.XPath, Using = "//body//div//div//td[5]")]
        public IWebElement ClearFilterBuilding;

        [FindsBy(How = How.XPath, Using = "//body//div//div//td[6]")]
        public IWebElement ClearFilterLocation;
        [FindsBy(How = How.Id, Using = "lblTotalRows")]
        public IWebElement TotalRowsCount;
        [FindsBy(How = How.Id, Using = "lblPlantID")]
        public IWebElement PlantId;

        [FindsBy(How = How.Id, Using = "lblFormulaID")]
        public IWebElement FormulaId;

        [FindsBy(How = How.XPath, Using = "//tr[2]//td[2]//div[1]//span[1]")]
        public IWebElement RankingNum1;

        [FindsBy(How = How.XPath, Using = "//table[@id='grdRework']//tr[@align='left']")]
        public IWebElement TableRowCont;
        [FindsBy(How = How.XPath, Using = "//tr[2]//td[10]//span[1]")]
        public IWebElement Comment;

        [FindsBy(How = How.XPath, Using = "//table[@id='grdRework']")]
        public IWebElement Table;


        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsView_BatchID']")]
        public IWebElement BatchIdTextBox;

        [FindsBy(How = How.XPath, Using = "(//*[contains(text(),'C1900104')]//following::input[@type='submit'])[1]")]
        public IWebElement selectbutton;

        [FindsBy(How = How.XPath, Using = "//input[@id='GridView_Button1_0']")]
        public IWebElement selectbutton2;

        [FindsBy(How = How.XPath, Using = "//input[@id='Create']")]
        public IWebElement CreateBatch;

        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsViewMain_Edit']")]
        public IWebElement EditBtn;

        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsViewMain_btnAddRework']")]
        public IWebElement AddReworkBtn;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'Rank')]")]
        public IWebElement RankColumn;

        [FindsBy(How = How.XPath, Using = "//div[@class='bcc-tooltip bcc-info']")]
        public IWebElement QuestionMark;

        [FindsBy(How = How.XPath, Using = "//span[@class='bcc-tooltiptext']")]
        public IWebElement RankDetails;

        [FindsBy(How = How.XPath, Using = "//input[@id='btnOverview']")]
        public IWebElement OverviewBtn;

        [FindsBy(How = How.XPath, Using = "//span[@id='DetailsView_Status']")]
        public IWebElement Status;

        //***********//
        [FindsBy(How = How.XPath, Using = "(//span[@id='lblFormulaID'])[1]")]
        public IWebElement FormulaID;

        [FindsBy(How = How.XPath, Using = "//span[@id='lblTotalRows']")]
        public IWebElement totolRows;
        [FindsBy(How = How.XPath, Using = "//span[@id='lblTotalWeight']")]
        public IWebElement totolWeight;

        [FindsBy(How = How.XPath, Using = "(//span[@id='lblComments'])[8]")]
        public IWebElement WMSinCmnt;
        [FindsBy(How = How.XPath, Using = "(//span[@id='lblComments'])[1]")]
        public IWebElement LIMSinCmnt;
        [FindsBy(How = How.XPath, Using = "//span[@id='lblContainerID']")]
        public IWebElement Container1Record;

        [FindsBy(How = How.XPath, Using = "//input[@name='grdRework$ctl02$ctl00']")]
        public IWebElement AddButton;

        [FindsBy(How = How.XPath, Using = "//span[@id='DetailsViewMain_SKU1']")]
        public IWebElement AddedSKU;

        [FindsBy(How = How.XPath, Using = "//span[@id='DetailsViewMain_GVIngredients_lblLocLocationID_10']")]
        public IWebElement ReworkLOC;

        [FindsBy(How = How.XPath, Using = " //span[@id='DetailsViewMain_GVIngredients_lblLocLotNumber_10']")]
        public IWebElement ReworkLOT;

        [FindsBy(How = How.XPath, Using = "//span[@id='DetailsViewMain_GVIngredients_lblLocContainerID_10']")]
        public IWebElement ReworkCont;

        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsViewMain_Quantity']")]
        public IWebElement LotWeight;

        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsViewMain_GVIngredients_FormulaQTY_10']")]
        public IWebElement FormulaIdTextBox;

        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsViewMain_GVIngredients_Distribute_10']")]
        public IWebElement DistButton;

        [FindsBy(How = How.XPath, Using = " //span[@id='GVReworkComponents_IngredientID_0']")]
        public IWebElement IngredientList;

        [FindsBy(How = How.XPath, Using = "//select[@id='GVReworkComponents_MatchList_3']")]
        public IWebElement Match3;

        [FindsBy(How = How.XPath, Using = " //select[@id='GVReworkComponents_MatchList_4']")]
        public IWebElement Match4;

        [FindsBy(How = How.XPath, Using = "//select[@id='GVReworkComponents_MatchList_8']")]
        public IWebElement Match8;

        [FindsBy(How = How.XPath, Using = "//input[@id='Adjust']")]
        public IWebElement AdjustButton;

        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsViewMain_Update']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsViewMain_Back']")]
        public IWebElement BackButton;

        [FindsBy(How = How.XPath, Using = "//input[@id='btnRework']")]
        public IWebElement ReworkBtn;
        [FindsBy(How = How.XPath, Using = "//tr[2]//td[7]//span[1]")]
        public IWebElement SearchedLot;

        [FindsBy(How = How.XPath, Using = "//input[@id='btnStabilizing']")]
        public IWebElement StabilizingBtn;

        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsView_Next']")]
        public IWebElement NextBtnOnStabilizing;


        [FindsBy(How = How.XPath, Using = "//td[contains(text(),'Loc:')]//following-sibling::td[1]")]
        public IWebElement LocValue;


        [FindsBy(How = How.XPath, Using = "//td[contains(text(),'Lot:')]//following-sibling::td[1]")]
        public IWebElement LotValue;

        [FindsBy(How = How.XPath, Using = "//td[contains(text(),'Cont:')]//following-sibling::td[1]")]
        public IWebElement ContValue;

        [FindsBy(How = How.XPath, Using = "//span[@id='DetailsView_Label4']")]
        public IWebElement InstructionPageHeader;

        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsView_Edit']")]
        public IWebElement EditBtnOnStabilize;

        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsView_btnUpdate']")]
        public IWebElement EditModeStabilize;

        [FindsBy(How = How.XPath, Using = "//input[@id='Start']")]
        public IWebElement RealeseBtn;

        [FindsBy(How = How.XPath, Using = "//input[@id='btnPasteMixing']")]
        public IWebElement PasteMixingBtn;

        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsView_Back']")]
        public IWebElement PasteMixingStartBtn;

        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsView_Delete']")]
        public IWebElement UnReleasedBtn;

        [FindsBy(How = How.XPath, Using = "//input[@id='Undo']")]
        public IWebElement UndoBtn;

        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsViewMain_GVIngredients_DeleteIngredient_10']")]
        public IWebElement DeleteCheckBox;

        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsViewMain_SaveCancel']")]
        public IWebElement CancelBtn;
        [FindsBy(How = How.XPath, Using = "//input[@id='DetailsViewMain_Back']")]
        public IWebElement BackBtn;

        [FindsBy(How = How.Id, Using = "DetailsViewMain_SaveCancel")]
        public IWebElement UpdateCancelBtn;

        public void Goto()
        {
            Pages.Login.LogIn("login");
            Pages.TopNavigation.ClickReworkBtn();
        }

        public void Goto1()
        {
            clearFilterButton.Click();
        }

        public void GotoHome()
        {
            closeButton.Click();

        }
        
        public void ClickReworkBtn()
        {
            ReworkBtn.Click();
        }

        public bool IsAt()
        {
            return Browser.Title.Contains("Rework Lookup - Batch Process");

        }


        public void Contents()
        {
            Assert.IsTrue(Pages.Rework.datatable.Displayed);
            Assert.IsTrue(Pages.Rework.applyFilterButton.Displayed);
            Assert.IsTrue(Pages.Rework.clearFilterButton.Displayed);
            Assert.IsTrue(Pages.Rework.closeButton.Displayed);
            Assert.IsTrue(Pages.Rework.paginationSec.Displayed);
            Assert.IsTrue(Pages.Rework.DescriptionTextBox.Displayed);
            Assert.IsTrue(Pages.Rework.LotNumberTextBox.Displayed);
            Assert.IsTrue(Pages.Rework.FormulaIDTextBox.Displayed);
            Assert.IsTrue(Pages.Rework.BuildingIDTextBox.Displayed);
            Assert.IsTrue(Pages.Rework.LocationTextBox.Displayed);
            Assert.IsTrue(Pages.Rework.PlantLabel.Displayed);
            Assert.IsTrue(Pages.Rework.FormulaIDLabel.Displayed);
            Assert.IsTrue(Pages.Rework.TotalRowsLabel.Displayed);
            Assert.IsTrue(Pages.Rework.TotalWeightsLabel.Displayed);
           
        }
        public void FilteringByExistingSkuReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            string ActualSKu = UserData.SKU;
            string SearchedSKU = searchedSKU.Text;
            Assert.AreEqual(ActualSKu, SearchedSKU, "SKU filter value is "+ SearchedSKU);
          
        }
        public void FilteringByExistingDescriptionReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            string ActualDescription = UserData.Description;
            string SearchedDescription = searchedDescription.Text;
            Assert.AreEqual(ActualDescription, SearchedDescription, "Description filter value is:"+ SearchedDescription);
            
        }
        public void FilteringByExistingFormulaIDReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            string ActualFormulaID = UserData.FormulaID;
            string SearchedFormulaID = searchedFormulaID.Text;
            Assert.AreEqual(ActualFormulaID, SearchedFormulaID, "Formula ID value is:"+ SearchedFormulaID);
            
        }

        public void ClickCloseBtn()
        {
            closeButton.Click();
        }

        public void FilteringByExistingBuildingReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            string ActualBuilding = UserData.Building;
            string SearchedBuilding = searchedBuilding.Text;
            Assert.AreEqual(ActualBuilding, SearchedBuilding, "Building value is:"+ SearchedBuilding);
          
        }
        public void FilteringByExistingLocationReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            string ActualLocation = UserData.Location;
            string SearchedLocation = searchedLocation.Text;
            Assert.AreEqual(ActualLocation, SearchedLocation, "Location value is:"+ SearchedLocation);
           
        }
        public void FilteringByExistingLotReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            string ActualLot = UserData.LOT;
            string SearchedLot = searchedLOT.Text;
            Assert.AreEqual(ActualLot, SearchedLot, "LOT value is:"+ SearchedLot);
           
        }
        public void FilteringByExistingContainerReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            string ActualContainer = UserData.Container;
            string SearchedContainer = searchedContainer.Text;
            Assert.AreEqual(ActualContainer, SearchedContainer, "Container value is:"+ SearchedContainer);
           
        }
        public void FilteringByExistingSkuwithDescriptionReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            SKUTextBox.SendKeys(UserData.SKU);
            DescriptionTextBox.SendKeys(UserData.Description);
            applyFilterButton.Click();
            FilteringByExistingSkuReturnsTheCorrectData("Rework");
            FilteringByExistingDescriptionReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingSkuwithFormulaIdReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            SKUTextBox.SendKeys(UserData.SKU);
            FormulaIDTextBox.SendKeys(UserData.FormulaID);
           
            applyFilterButton.Click();
            FilteringByExistingSkuReturnsTheCorrectData("Rework");
            FilteringByExistingFormulaIDReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingSkuwithBuildingReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            SKUTextBox.SendKeys(UserData.SKU);
            BuildingIDTextBox.SendKeys(UserData.Building);
           
            applyFilterButton.Click();
            FilteringByExistingSkuReturnsTheCorrectData("Rework");
            FilteringByExistingBuildingReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingSkuwithLocationReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            SKUTextBox.SendKeys(UserData.SKU);
            LocationTextBox.SendKeys(UserData.Location);
           
            applyFilterButton.Click();
            FilteringByExistingSkuReturnsTheCorrectData("Rework");
            FilteringByExistingLocationReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingSkuwithLotReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            SKUTextBox.SendKeys(UserData.SKU);
            LotNumberTextBox.SendKeys(UserData.LOT);
           
            applyFilterButton.Click();
            FilteringByExistingSkuReturnsTheCorrectData("Rework");
            FilteringByExistingLotReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingSkuwithContainerReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            SKUTextBox.SendKeys(UserData.SKU);
            ContainerTextBox.SendKeys(UserData.Container);
           
            applyFilterButton.Click();
            FilteringByExistingSkuReturnsTheCorrectData("Rework");
            FilteringByExistingContainerReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingDescriptionwithLotReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            DescriptionTextBox.SendKeys(UserData.Description);
            LotNumberTextBox.SendKeys(UserData.LOT);
           
            applyFilterButton.Click();
            FilteringByExistingDescriptionReturnsTheCorrectData("Rework");
            FilteringByExistingLotReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingDescriptionwithFormulaIDReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            DescriptionTextBox.SendKeys(UserData.Description);
            FormulaIDTextBox.SendKeys(UserData.FormulaID);
           
            applyFilterButton.Click();
            FilteringByExistingDescriptionReturnsTheCorrectData("Rework");
            FilteringByExistingFormulaIDReturnsTheCorrectData("Rework");

        }
        public void FilteringByExistingDescriptionwithBuildingReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            DescriptionTextBox.SendKeys(UserData.Description);
            BuildingIDTextBox.SendKeys(UserData.Building);
           
            applyFilterButton.Click();
            FilteringByExistingDescriptionReturnsTheCorrectData("Rework");
            FilteringByExistingBuildingReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingDescriptionwithLocationReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            DescriptionTextBox.SendKeys(UserData.Description);
            LocationTextBox.SendKeys(UserData.Location);
           
            applyFilterButton.Click();
            FilteringByExistingDescriptionReturnsTheCorrectData("Rework");
            FilteringByExistingLocationReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingDescriptionwithContainerReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            DescriptionTextBox.SendKeys(UserData.Description);
            ContainerTextBox.SendKeys(UserData.Container);
           
            applyFilterButton.Click();
            FilteringByExistingDescriptionReturnsTheCorrectData("Rework");
            FilteringByExistingContainerReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingLotwithFormulaIdReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            LotNumberTextBox.SendKeys(UserData.LOT);
            FormulaIDTextBox.SendKeys(UserData.FormulaID);
           
            applyFilterButton.Click();
            FilteringByExistingLotReturnsTheCorrectData("Rework");
            FilteringByExistingFormulaIDReturnsTheCorrectData("Rework");

        }
        public void FilteringByExistingLotwithBuildingReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            LotNumberTextBox.SendKeys(UserData.LOT);
            BuildingIDTextBox.SendKeys(UserData.Building);
           
            applyFilterButton.Click();
            FilteringByExistingLotReturnsTheCorrectData("Rework");
            FilteringByExistingBuildingReturnsTheCorrectData("Rework");

        }
        public void FilteringByExistingLotwithLocationReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            LotNumberTextBox.SendKeys(UserData.LOT);
            LocationTextBox.SendKeys(UserData.Location);
           
            applyFilterButton.Click();
            FilteringByExistingLotReturnsTheCorrectData("Rework");
            FilteringByExistingLocationReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingLotwithContainerReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            LotNumberTextBox.SendKeys(UserData.LOT);
            ContainerTextBox.SendKeys(UserData.Container);
           
            applyFilterButton.Click();
            FilteringByExistingLotReturnsTheCorrectData("Rework");
            FilteringByExistingContainerReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingFormulaIDwithBuildingReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            FormulaIDTextBox.SendKeys(UserData.FormulaID);
            BuildingIDTextBox.SendKeys(UserData.Building);
           
            applyFilterButton.Click();
            FilteringByExistingFormulaIDReturnsTheCorrectData("Rework");
            FilteringByExistingBuildingReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingFormulaIDwithLocationReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            FormulaIDTextBox.SendKeys(UserData.FormulaID);
            LocationTextBox.SendKeys(UserData.Location);
           
            applyFilterButton.Click();
            FilteringByExistingFormulaIDReturnsTheCorrectData("Rework");
            FilteringByExistingLocationReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingFormulaIDwithContainerReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            FormulaIDTextBox.SendKeys(UserData.FormulaID);
            ContainerTextBox.SendKeys(UserData.Container);
           
            applyFilterButton.Click();
            FilteringByExistingFormulaIDReturnsTheCorrectData("Rework");
            FilteringByExistingContainerReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingBuildingwithLocationReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            BuildingIDTextBox.SendKeys(UserData.Building);
            LocationTextBox.SendKeys(UserData.Location);
           
            applyFilterButton.Click();
            FilteringByExistingBuildingReturnsTheCorrectData("Rework");
            FilteringByExistingLocationReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingBuildingwithContainerReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            BuildingIDTextBox.SendKeys(UserData.Building);
            ContainerTextBox.SendKeys(UserData.Container);
           
            applyFilterButton.Click();
            FilteringByExistingBuildingReturnsTheCorrectData("Rework");
            FilteringByExistingContainerReturnsTheCorrectData("Rework");
        }
        public void FilteringByExistingLocationwithContainerReturnsTheCorrectData(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            LocationTextBox.SendKeys(UserData.Location);
            ContainerTextBox.SendKeys(UserData.Container);
           
            applyFilterButton.Click();
            FilteringByExistingLocationReturnsTheCorrectData("Rework");
            FilteringByExistingContainerReturnsTheCorrectData("Rework");
        }
        public void ClickApplyFilterBtn()
        {
            applyFilterButton.Click();
        }
        public void SKUFilterFunctionality(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            SKUTextBox.SendKeys(UserData.SKU);
            ClickApplyFilterBtn();
            FilteringByExistingSkuReturnsTheCorrectData("Rework");

        }
        public void DescriptionFilterFunctionality(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            DescriptionTextBox.SendKeys(UserData.Description);
            ClickApplyFilterBtn();
            FilteringByExistingDescriptionReturnsTheCorrectData("Rework");
        }
        public void FormulaIdFilterFunctionality(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            FormulaIDTextBox.SendKeys(UserData.FormulaID);
            ClickApplyFilterBtn();
            FilteringByExistingFormulaIDReturnsTheCorrectData("Rework");
        }
        public void LotFilterFunctionality(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            LotNumberTextBox.SendKeys(UserData.LOT);
            ClickApplyFilterBtn();
            FilteringByExistingLotReturnsTheCorrectData("Rework");
        }
        public void FilteringByNonExistingBuildingReturnsZeroRows(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            BuildingIDTextBox.SendKeys(UserData.InvalidBuilding);
            ClickApplyFilterBtn();
            string ExpectedMsg = "No records found.";
            string ActualMsg = NoRecordFound.Text;
            Assert.AreEqual(ExpectedMsg, ActualMsg, "Message displayed as:"+ ActualMsg);
            
        }
        public void FilteringByNonExistingLocationReturnsNoRecordsfoundMessage(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            LocationTextBox.SendKeys(UserData.InvalidLocation);
            ClickApplyFilterBtn();
            string ExpectedMsg = "No records found.";
            string ActualMsg = NoRecordFound.Text;
            Assert.AreEqual(ExpectedMsg, ActualMsg, "Message displayed as:" + ActualMsg);
            
        }
        public void ContainerFilterFunctionality(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            ContainerTextBox.SendKeys(UserData.Container);
            ClickApplyFilterBtn();
            FilteringByExistingContainerReturnsTheCorrectData("Rework");
        }
        public void GetTotalWeight(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            SKUTextBox.SendKeys(UserData.SKU);
            ClickApplyFilterBtn();
            string ActualTotalWeight = TotalWt.Text;
            string ExpectedWeight = "42,000";
            Assert.AreEqual(ActualTotalWeight, ExpectedWeight, "Actual weight matched with expected weight");

        }
        public void Reworkwt()
        {
            char[] charsToTrim3 = { 'R', 'e', 'w', 'o', 'r', 'k', '(', ')', '.', 'l', 'b', 's', ' ' };
            string ReworkLabelwt = ReworkBtnwt.GetAttribute("value");
            string ReworkLabelwtAfter = ReworkLabelwt.Trim(charsToTrim3);
            ReworkBtnwt.Click();
            string TotalWtOnReworkLookup = TotalWt.Text;
            Assert.AreNotEqual(ReworkLabelwtAfter, TotalWtOnReworkLookup, "Actual weight matched with expected weight");

        }
        public void NavigatedToTheNextPage()
        {

            PaginationBtn.Click();
            Boolean status = PaginationBtn1.Enabled;
            Console.WriteLine("Actual status" + status);

        }
        public void FilteringByExistingvalueandClearFilterReturnsAllDataVanish(string testName)
        {

            var UserData = DataAccess.GetTestData(testName);
            SKUTextBox.SendKeys(UserData.SKU);
            DescriptionTextBox.SendKeys(UserData.Description);
            LotNumberTextBox.SendKeys(UserData.ClearFilterLot);
            BuildingIDTextBox.SendKeys(UserData.Building);
            FormulaIDTextBox.SendKeys(UserData.ClearFilterFormulaID);
            LocationTextBox.SendKeys(UserData.Location);
            ContainerTextBox.SendKeys(UserData.Container);
           
            ClickApplyFilterBtn();
            clearFilterButton.Click();
           
            string ExpectedAfterClearFiterSKU = "";
            string ExpectedAfterClearFiterDescription = "";
            string ExpectedAfterClearFiterLot = "";
            string ExpectedAfterClearFiterBuilding = "";
            string ExpectedAfterClearFiterLocation = "";
            string ExpectedAfterClearFiterContainer = "";
            string ExpectedAfterClearFiterFormula = "";
            
            string ActualAfterClearFiterSKU = SKUTextBox.Text;
            string ActualAfterClearFiterDescription = DescriptionTextBox.Text;
            string ActualAfterClearFiterLot = LotNumberTextBox.Text;
            string ActualAfterClearFiterBuilding = BuildingIDTextBox.Text;
            string ActualAfterClearFiterLocation = LocationTextBox.Text;
            string ActualAfterClearFiterContainer = ContainerTextBox.Text;
            string ActualAfterClearFiterFormula = FormulaIDTextBox.Text;

            Assert.AreEqual(ExpectedAfterClearFiterSKU, ActualAfterClearFiterSKU, "Clear Filter Functionality Works Properly");
            Assert.AreEqual(ExpectedAfterClearFiterDescription, ActualAfterClearFiterDescription, "Clear Filter Functionality Works Properly");
            Assert.AreEqual(ExpectedAfterClearFiterLot, ActualAfterClearFiterLot, "Clear Filter Functionality Works Properly");
            Assert.AreEqual(ExpectedAfterClearFiterBuilding, ActualAfterClearFiterBuilding, "Clear Filter Functionality Works Properly");
            Assert.AreEqual(ExpectedAfterClearFiterLocation, ActualAfterClearFiterLocation, "Clear Filter Functionality Works Properly");
            Assert.AreEqual(ExpectedAfterClearFiterContainer, ActualAfterClearFiterContainer, "Clear Filter Functionality Works Properly");
            Assert.AreEqual(ExpectedAfterClearFiterFormula, ActualAfterClearFiterFormula, "Clear Filter Functionality Works Properly");

        }
        public void AsPerSearchDataReturnRowCorret(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            SKUTextBox.SendKeys(UserData.SKU);
            ClickApplyFilterBtn();

            string GetCount = TotalRowsCount.Text;
            int TotalRowsnumber = int.Parse(GetCount);
           

            IList<IWebElement> rows = Table.FindElements(By.TagName("tr"));
            int SubtractTheHeader = 3;
            int TableRowCount = rows.Count;
           
            int finalrowCountWithData = TableRowCount + SubtractTheHeader;
            Assert.AreEqual(TotalRowsnumber, finalrowCountWithData, "Table rows count match with filtered data's row count:");
        }
        public void VerifyFormulaId()
        {
            Pages.Login.LogIn("login");
            Browser.FilterFormulaReworkLookup();
           
            string ExpectedPlantNm = "CH";
            string ExpectedFormulaIdNm = "CIOD-CHOCHUNKS";
            string ExpectedRowsCount = "21";
            string ExpectedTotalWt = "39,010";

            string ExpectedRankingValue = "1";
            string ActualPlantNm = PlantId.Text;
            string ActualFormula = FormulaId.Text;
            string ActualRowsCount = TotalRowsCount.Text;
            string ActualTotalWt = TotalWeightsValue.Text;
            string ActualRankingId = RankingNum1.Text;

            Assert.AreEqual(ExpectedFormulaIdNm, ActualFormula, "Expected Formula Id match with acutal formula Id");
            Assert.AreEqual(ExpectedPlantNm, ActualPlantNm, "Expected Plant name match with acutal plant name");
            Assert.AreEqual(ExpectedFormulaIdNm, ActualFormula, "Expected Formula Id match with acutal formula Id");
            Assert.AreEqual(ExpectedRowsCount, ActualRowsCount, "Expected rows count match with actual rows count");
            Assert.AreEqual(ExpectedTotalWt, ActualTotalWt, "Expected weight match with actual total weight");
           
        }

        public void VerifyRowsCount()
        {

            IList<IWebElement> rows = Table.FindElements(By.TagName("tr"));
            int SubtractTheHeader = 2;
            int TableRowCount = rows.Count;
            int finalrowCountWithData = TableRowCount - SubtractTheHeader;

            for (int i = 2; i <= finalrowCountWithData; i++)
            {
                if (i >= 2)
                {
                    string RankColumn = Table.FindElement(By.XPath("//tr[" + i + "]//td[2]//div[1]")).Text;
                    string ExpectedRank = "2";
                    {
                        if (RankColumn.Equals(ExpectedRank))
                        {
                            string Formula = Table.FindElement(By.XPath("//tr[" + i + "]//td[6]")).Text;
                            string ExpectedFormulaNm = FormulaId.Text;

                            Assert.AreEqual(Formula, ExpectedFormulaNm, "Rows with first three characters of the filtered Formula ID not display a ranking value of 2");
                        }
                    }
                }
            }
        }

        public void FilterVerifyReworkLookupPageContents(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            LotNumberTextBox.SendKeys(UserData.AcessReworkPageLot);
            ClickApplyFilterBtn();

            string ExpectedPlantNm = "CH";
            string ExpectedFormulaIdNm = "All Rework Formulas";
            string ExpectedRowsCount = "32";
            string ExpectedTotalWt = "56,700";
            string ExpectedComment = "WMS: mix with Lancaster ICC";

            string ActualPlantNm = PlantId.Text;
            string ActualFormula = FormulaId.Text;
            string ActualRowsCount = TotalRowsCount.Text;
            string ActualTotalWt = TotalWeightsValue.Text;
            string actualComment = Comment.Text;

            Assert.AreEqual(ExpectedPlantNm, ActualPlantNm, "Expected Plant name match with acutal plant name");
            Assert.AreEqual(ExpectedFormulaIdNm, ActualFormula, "Expected Formula Id match with acutal formula Id");
            Assert.AreEqual(ExpectedRowsCount, ActualRowsCount, "Expected rows count match with actual rows count");
            Assert.AreEqual(ExpectedTotalWt, ActualTotalWt, "Expected weight match with actual total weight");
            Assert.AreEqual(ExpectedComment, actualComment, "Expected comment match with acutal comment");

        }

        public void VerifyRankingFunctionality(string testName)

        {

            var UserData = DataAccess.GetTestData(testName);
            BatchIdTextBox.SendKeys(UserData.Ranking);
            BatchIdTextBox.SendKeys(Keys.Enter);

            // Browser.ExplicityWait(CreateBatch);
            System.Threading.Thread.Sleep(5000);

            CreateBatch.Click();
            System.Threading.Thread.Sleep(5000);
            // Browser.ExplicityWait(EditBtn);

            EditBtn.Click();
            System.Threading.Thread.Sleep(8000);
            //  Browser.ExplicityWait(AddReworkBtn);
            AddReworkBtn.Click();

            System.Threading.Thread.Sleep(5000);

            string ActualColumnName = RankColumn.Text;
            string ExpectedColumnName = "Rank";
            Assert.AreEqual(ExpectedColumnName, ActualColumnName, "Column Name Displayed as:"+ ActualColumnName);
           
            string ActualIcon = QuestionMark.Text;
            string ExpectedIcon = "?";
            Assert.AreEqual(ActualIcon, ExpectedIcon, "Question mark icon get displayed");
            System.Threading.Thread.Sleep(5000);
            QuestionMark.Click();
           
            LotNumberTextBox.SendKeys(UserData.RankingLotId);
            Browser.ImplicitWiat();
            applyFilterButton.Click();
           

            IList<IWebElement> rows = Table.FindElements(By.TagName("tr"));
            int SubtractTheHeader = 2;
            int TableRowCount = rows.Count;
            int finalrowCountWithData = TableRowCount - SubtractTheHeader;
          

            for (int i = 2; i <= finalrowCountWithData; i++)
            {
                if (i >= 2)
                {
                    string ActulRankColumn = Table.FindElement(By.XPath("//tr[" + i + "]//td[2]//div[1]")).Text;
                    
                    if (ActulRankColumn.Equals("1") || ActulRankColumn.Equals("2") || ActulRankColumn.Equals("3"))
                    {
                        Console.WriteLine(ActulRankColumn + " :Rank is in between 1 , 2 or 3:");

                    }
                    else
                    {
                        Console.WriteLine(ActulRankColumn + " :Rank is in not between 1 , 2 or 3:");
                    }
                }
            }
           
            Pages.Rework.GotoHome();
            System.Threading.Thread.Sleep(5000);
            CancelBtn.Click();
            System.Threading.Thread.Sleep(8000);
            BackBtn.Click();
        }

        public void VerifyReworkRankingFunctionality(string testName)
        {
            
            string ActualFormulaID = FormulaID.Text;
            string ExpectedFormulaID = "All Rework Formulas";
            Assert.AreEqual(ExpectedFormulaID, ActualFormulaID, "Formula ID is match with " + ActualFormulaID);

            string ActualTotalRows = totolRows.Text;
            string ExpectedTotalRows = "410";
            Assert.AreEqual(ExpectedTotalRows, ActualTotalRows, "Total rows match with " + ActualTotalRows);
           
            string ActualTotalWeight = totolWeight.Text;
            string ExpectedTotalWeight = "697,003";
           // Assert.AreEqual(ExpectedTotalWeight, ActualTotalWeight, "Total weight match with " + ActualTotalWeight);
           
           

        }

        public void ConfirmFilteringIsWorking(string testName)
        {
            var UserData = DataAccess.GetTestData(testName);
            LotNumberTextBox.SendKeys(UserData.LOT);
            ClickApplyFilterBtn();

        }

        public void LotSearchResult()
        {
            string ActualLotID = SearchedLot.Text;
            string ExpectedLotID = "C182001";
            Assert.AreEqual(ActualLotID, ExpectedLotID,"Lot Id is match correctly");

            string ActualRowCount = TotalRowsCount.Text;
            string ExpectedRowCount = "22";
            Assert.AreEqual(ActualRowCount, ExpectedRowCount, "Row count get match correctly");
         
            string ActualTotalWt = TotalWeightsValue.Text;
            string ExpectedTotalWt = "44,000";
            Assert.AreEqual(ActualTotalWt, ExpectedTotalWt, "Lot Id get matched correctly");
            
            string actualComment = Comment.Text;
            string ExpectedComment = "WMS: fat out of spec - use in production\r\nLIMS: Out Of Spec for Fat";
            Assert.AreEqual(actualComment, ExpectedComment, "Comments are matched correctly");

            clearFilterButton.Click();
            string actualLotTxtField = LotNumberTextBox.Text;
            string ExpectedLotTxtField = "";
            Assert.AreEqual(actualLotTxtField, ExpectedLotTxtField, "Lot C182001 Is removed from lot # field.");
            closeButton.Click();
             
        }

        public void ReworkStabilizing(string testName)
        {
            StabilizingBtn.Click();
            var UserData = DataAccess.GetTestData(testName);
            BatchIdTextBox.SendKeys(UserData.StabilizingBatchId);
            BatchIdTextBox.SendKeys(Keys.Enter);
            NextBtnOnStabilizing.Click();
            Assert.IsTrue(Pages.Rework.InstructionPageHeader.Displayed);
            NextBtnOnStabilizing.Click();

            string ActualLocValue = LocValue.Text;
            string ExpectedLocValue = "FL11";
            Assert.AreEqual(ExpectedLocValue, ActualLocValue, "Loc values shows for rework row " + ActualLocValue);
            string ActualLotValue = LotValue.Text;
            string ExpectedLotValue = "	C162849";
            Assert.AreEqual(ExpectedLotValue, ActualLotValue, "Lot values shows for rework row " + ActualLotValue);
            string ActualContValue = ContValue.Text;
            string ExpectedContValue = "CH950332";
            Assert.AreEqual(ExpectedContValue, ActualContValue, "Cont values shows for rework row " + ActualContValue);

            EditBtnOnStabilize.Click();
            string ActualEditLocValue = LocValue.Text;
            string ExpectedEditLocValue = "FL11";
            Assert.AreEqual(ActualEditLocValue, ExpectedEditLocValue, "Loc values shows in Edit mode for rework row " + ActualEditLocValue);
           
            string ActualEditLotValue = LotValue.Text;
            string ExpectedEditLotValue = "C162617";
            Assert.AreEqual(ActualEditLotValue, ExpectedEditLotValue, "Lot values shows in Edit mode for rework row " + ActualEditLotValue);
            
            string ActualEditContValue = ContValue.Text;
            string ExpectedEditContValue = "CH952693";
            Assert.AreEqual(ExpectedEditContValue, ActualEditContValue, "Cont values shows in Edit mode for rework row" + ActualEditContValue);
            

        }
        public void BatchedIsReleased(string testName)
        {

            var UserData = DataAccess.GetTestData(testName);
            BatchIdTextBox.SendKeys(UserData.ReworkRanking);
            BatchIdTextBox.SendKeys(Keys.Enter);
           
            string ActualStatus = Status.Text;
            string ExpectedStatus = "Not Released";
            Assert.AreEqual(ExpectedStatus, ActualStatus, "Batch Status is displayed as " + ActualStatus);
           
            RealeseBtn.Click();

            string ActualValue = BatchIdTextBox.Text;
            string ExpectedValue = "";
            Assert.AreEqual(ExpectedValue, ActualValue, "Batch record removed from Overview page  " + ActualValue);
           
        }
    
        public void UnReleaseBatch(string testName)
        {
             System.Threading.Thread.Sleep(3000);
            Browser.ExplicityWait(UnReleasedBtn);
            var UserData = DataAccess.GetTestData(testName);
            UnReleasedBtn.Click();
            System.Threading.Thread.Sleep(3000);
            Browser.alert();
            System.Threading.Thread.Sleep(5000);
           
            BatchIdTextBox.SendKeys(UserData.BatchID);
            BatchIdTextBox.SendKeys(Keys.Enter);
            Browser.ExplicityWait(Status);
            System.Threading.Thread.Sleep(3000);
            string ActualStatus = Status.Text;
            string ExpectedStatus = "Not Released";
            Assert.AreEqual(ExpectedStatus, ActualStatus, "Batch Status is displayed as " + ActualStatus);


        }       
    }
}

