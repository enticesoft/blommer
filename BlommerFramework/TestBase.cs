﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using System;
using System.IO;
using System.Threading;

namespace BlommerFramework
{
    [TestFixture]
    public class TestBase
    {
        public static IWebDriver driver= null;
        public static ExtentReports _extent = null;
        public static ExtentTest _test = null;

        [OneTimeSetUp]
        public static void Initialize()
        {
            //UserGenerator.Initialize();
            CreateReportDirectory();
            Browser.NavigateTo();
        }

        //To create report directory and add HTML report into it
        public static void CreateReportDirectory()
        {
            _extent = new ExtentReports();
            var dir = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", "");
            DirectoryInfo di = Directory.CreateDirectory(dir + "\\TestResults");
            var htmlReporter = new ExtentHtmlReporter(dir + "\\TestResults\\AutomationReport.html");
            _extent.AddSystemInfo("Environment", "Test");
            _extent.AddSystemInfo("Username", "Xcelacore");
            _extent.AttachReporter(htmlReporter);
        }

        ///Getting the name of current running test to extent report
        [SetUp]
        public void BeforeTest()
        {
            try
            {
                _test = _extent.CreateTest(TestContext.CurrentContext.Test.Name);
               
               

            }
            catch (Exception e)
            {
                throw (e);
            }

        }

        /// Finish the execution and logging the details into HTML report
        [TearDown]
        public void AfterTest()
        {
            try
            {
                var status = TestContext.CurrentContext.Result.Outcome.Status;
                var stacktrace = "" + TestContext.CurrentContext.Result.StackTrace + "";
                var errorMessage = TestContext.CurrentContext.Result.Message;
                Status logstatus;
                switch (status)
                {
                    case TestStatus.Failed:
                        logstatus = Status.Fail;
                        string screenShotPath = Capture(Browser.webDriver, TestContext.CurrentContext.Test.Name);
                        _test.Log(logstatus, "Test ended with " + logstatus + " – " + errorMessage);
                        _test.Log(logstatus, "Snapshot below: " + _test.AddScreenCaptureFromPath(screenShotPath));
                        break;
                    case TestStatus.Skipped:
                        logstatus = Status.Skip;
                        _test.Log(logstatus, "Test ended with " + logstatus);
                        break;
                    default:
                        logstatus = Status.Pass;
                        _test.Log(logstatus, "Test ended with " + logstatus);
                        break;
                }
            }
            catch (Exception e)
            {
                throw (e);
            }

        }

        /// To capture the screenshot for extent report and return actual file path        
        private string Capture(IWebDriver driver, string screenShotName)
        {
            string localpath = "";
            try
            {
                Thread.Sleep(4000);
                ITakesScreenshot ts = (ITakesScreenshot)driver;
                Screenshot screenshot = ts.GetScreenshot();
                string pth = System.Reflection.Assembly.GetCallingAssembly().CodeBase;
                var dir = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", "");
                DirectoryInfo di = Directory.CreateDirectory(dir + "\\DefectScreenshots\\");
                string finalpth = pth.Substring(0, pth.LastIndexOf("bin")) + "\\DefectScreenshots\\" + screenShotName + ".png";
                localpath = new Uri(finalpth).LocalPath;
                screenshot.SaveAsFile(localpath);
            }
            catch (Exception e)
            {
                throw (e);
            }
            return localpath;
        }

        ///To flush extent report
        ///To quit driver instance
        [OneTimeTearDown]
        public static void TestFixtureTearDown()
        {
            try
            {
                _extent.Flush();
            }
            catch (Exception e)
            {
                throw (e);
            }
            Browser.webDriver.Close();
            Browser.webDriver.Quit();
            Browser.webDriver.Dispose();
        }
    }

   

    }
