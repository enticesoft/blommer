﻿using System;
using System.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;

namespace BlommerFramework
{
    public class Browser
    {
        public IWebDriver WebDriver { get; set; }
        public string environmentURL { get; set; }

        public static IWebDriver webDriver;

        private static string baseUrl = "http://bccqa-ws.ch.blommer.com/BatchProcess/Default.aspx"; //use app config for determining environment

        //public static IWebDriver webDriver = new ChromeDriver();
        //  public static IWebDriver webDriver = new InternetExplorerDriver();
        //public static IWebDriver webDriver = new FirefoxDriver();

        public Browser(IWebDriver webDriver)
        {
            environmentURL = ConfigurationManager.AppSettings["Base_URL"];
            WebDriver = webDriver;
        }

        public static void Initialize()
        {

            Goto("");
        }

        public static void NavigateTo()
        {
            string browser = ConfigurationManager.AppSettings["Browser"];

            switch (browser)
            {
                case "Firefox":
                    webDriver = new FirefoxDriver();
                    break;
                case "Chrome":
                    webDriver = new ChromeDriver();
                    break;
                case "IE":
                    webDriver = new InternetExplorerDriver();
                    break;
                case "Edge":
                    webDriver = new EdgeDriver();
                    break;
            }

            webDriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["Base_URL"]);

        }

        public static string Title
        {
            get { return webDriver.Title; }
        }

        public static string CurrentURL
        {
            get { return webDriver.Url; }
        }

        public static ISearchContext Driver
        {
            get { return webDriver; }
        }

        public static IWebDriver _Driver
        {
            get { return webDriver; }
        }

        public static IWebDriver driver
        {
            get { return webDriver; }
        }

        public static void Goto(string url)
        {
            webDriver.Url = baseUrl + url;
        }

        public static void Close()
        {
            webDriver.Close();
        }

        public static void ImplicitWiat()
        {

            //IWebDriver driver = new ChromeDriver();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(10000);
        }

        public static void ExplicityWait(IWebElement element)
        {
           
            WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromMilliseconds(20000));
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
        }

        public static void ExplicityWait(IWebElement element)
        {            
            ExplicityWait(element, 30000);
        }

        public static void ExplicityWait2(IWebElement element)
        {

            WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.ElementExists(By.Id("Adjust")));
			
		}

        public static void ExplicityWait(IWebElement element, int waitTime)
        {
            WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromMilliseconds(waitTime));
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
        }

        public static void alert()

        {
            IAlert simpleAlert = webDriver.SwitchTo().Alert();
            simpleAlert.Accept();
        }

        public static void FilterFormulaReworkLookup()
        {
            webDriver.Url = "http://bccqa-ws.ch.blommer.com/BatchProcess/App_Common/ReworkLookup.aspx?FormulaID=CIOD-CHOCHUNKS";
        }

        public static void LoginURL()
        {
            webDriver.Url = "http://bccqa-ws.ch.blommer.com/BatchProcess/Default.aspx";

            Browser.ImplicitWiat();

        }

        public static void TestInitializerURL()
        {
            webDriver.Url = "http://bccqa-ws.ch.blommer.com/TestInitializer/";


        }
    }
}