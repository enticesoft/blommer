﻿using BlommerFramework;
using NUnit.Framework;

namespace BlommerTests
{
    [TestFixture]
    public class SmokeTests : TestBase
    {

      
        [Test, Order(1)]
        public void CanLoginAsUser()

        {
            Pages.Login.LogIn("login");
            Assert.IsTrue(Pages.TopNavigation.overviewBtn.Displayed);
        }

        [Test, Order(2)]

        public void NavigateOnBatchOverviewPage()
        {
            Pages.TopNavigation.ClickOverviewBtn();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Overview.IsAt());
            Browser.ImplicitWiat();
        }

        [Test, Order(3)]
        public void DetailsBatchInfoDisplay()
        {
            Pages.Overview.DetailsBatchInfo();
            Assert.IsTrue(Pages.Overview.BatchDetails.Displayed);
        }
        
        [Test, Order(4)]
        public void CreateEditBatchOpen()
        {
            Pages.Overview.CreateEditBatch();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Overview.BatchMaintanancePage());
        }
        [Test, Order(5)]
        public void AfterclickonBackNavigatetoBatchOverviewPage()
        {          
            Pages.Overview.BackButton();
            System.Threading.Thread.Sleep(8000);
           // Pages.Overview.Xyz();
            Assert.IsTrue(Pages.Overview.BatchOverviewPage.Displayed);
        }
        [Test, Order(6)]
        public void NavigateOnBatchProcessPage()
        {
            Pages.Overview.MainMenuButton();
            Assert.IsTrue(Pages.TaskList.IsAt());
        }
        [Test, Order(7)]
        public void NavigateOnPasteMixingBatchPage()
        {
            Pages.BatchProcess.PasteMixingTab();
            Assert.IsTrue(Pages.BatchProcess.PasteMixingBatchPage.Displayed);
        }
        [Test, Order(8)]
        public void DetailsBatchInfoOnPasteMixingBatchPage()
        {
            Pages.Overview.DetailsBatchInfo();
            Assert.IsTrue(Pages.Overview.BatchDetails.Displayed);
        }

        [Test, Order(9)]
        public void PasteMixingIngredientspageopen()
        {
            Pages.BatchProcess.Ingredients();
            Assert.IsTrue(Pages.BatchProcess.PasteMixingIngredientsPage());

        }
        [Test, Order(10)]
        public void NavigateOnBatchProcessfromPasteMixingPage()
        {
            Pages.Overview.MainMenuButton();
            Assert.IsTrue(Pages.TaskList.IsAt());
        }
        [Test, Order(11)]
        public void NavigateOnRefiningBatchPage()
        {
            Pages.BatchProcess.Refining();
            Assert.IsTrue(Pages.BatchProcess.RefiningBatchPage());
        }
        [Test, Order(12)]
        public void DetailsBatchInfoOnRefiningBatchPage()
        {
            Pages.Overview.RefiningDetailsBatchInfo();
            Assert.IsTrue(Pages.Overview.BatchDetails.Displayed);
        }
        [Test, Order(13)]
        public void NavigateOnBatchProcessfromRefiningPage()
        {
            Pages.Overview.MainMenuButton();
            Assert.IsTrue(Pages.TaskList.IsAt());
        }
        [Test, Order(14)]
        public void NavigateOnStabilizingBatchPage()
        {
            Pages.BatchProcess.Stabilizing();
            Assert.IsTrue(Pages.BatchProcess.StabilizingBatchPage());
        }
        [Test, Order(15)]
        public void DetailsBatchInfoOnStabilizingBatchPage()
        {
            Pages.Overview.DetailsBatchInfo();
            Assert.IsTrue(Pages.Overview.BatchDetails.Displayed);
        }
        [Test, Order(16)]
        public void NavigateOnBatchProcessfromStabilizingPage()
        {
            Pages.Overview.MainMenuButton();
            Assert.IsTrue(Pages.TaskList.IsAt());
            Pages.Login.Logout();
            
        }
    }


}
