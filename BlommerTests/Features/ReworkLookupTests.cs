﻿using BlommerFramework;
using NUnit.Framework;

namespace BlommerTests.Features
{
    [TestFixture]
    class ReworkLookupTests : TestBase
    {
        [Test, Order(1)]
        public void AvailableContentsonReworkLookupPage()
        {
            Pages.Rework.Goto();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.Contents();
           
        }

        [Test, Order(2)]
        public void CanAccessReworkLookupPage()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.ClickCloseBtn();
            Pages.Login.Logout();
        }

        [Test, Order(3)]
        public void ClickOnCloseBtnandNavOnHomePage()
        {
            Pages.Login.LogIn("login");
            Pages.TopNavigation.ClickReworkBtn();
            Assert.IsTrue(Pages.Rework.IsAt());
            Assert.IsTrue(Pages.Rework.closeButton.Displayed);
            Pages.Rework.ClickCloseBtn();
            Assert.IsTrue(Pages.TaskList.IsAt());
            Assert.IsTrue(Pages.TopNavigation.reworkBtn.Displayed);
            Pages.Login.Logout();
           
        }

        [Test, Order(4)]
        public void ConfirmFilteringIsWorking()
        {
            Browser.LoginURL();
            Pages.Login.LogIn("login");
            Pages.Rework.ClickReworkBtn();
            Pages.Rework.ConfirmFilteringIsWorking("SmokeRework");
            Pages.Rework.LotSearchResult();
            Assert.IsTrue(Pages.TaskList.IsAt());
            Pages.Login.Logout();
           
        }

        [Test, Order(5)]
        public void FilterApplyAndVerifyContents()
        {
            Pages.Rework.Goto();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilterVerifyReworkLookupPageContents("Rework");
           
        }

        [Test, Order(6)]
        public void FilteringByExistingBuildingwithContainerReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingBuildingwithContainerReturnsTheCorrectData("Rework");
           
        }

        [Test, Order(7)]
        public void FilteringByExistingBuildingwithLocationReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingBuildingwithLocationReturnsTheCorrectData("Rework");
           
        }

        [Test, Order(8)]
        public void FilteringByExistingContainerReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.ContainerFilterFunctionality("Rework");
           
        }

        [Test, Order(9)]
        public void FilteringByExistingDescriptionReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.DescriptionFilterFunctionality("Rework");
           
        }

        [Test, Order(10)]
        public void FilteringByExistingDescriptionwithBuildingReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingDescriptionwithBuildingReturnsTheCorrectData("Rework");
           
        }

        [Test, Order(11)]
        public void FilteringByExistingDescriptionwithContainerReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingDescriptionwithContainerReturnsTheCorrectData("Rework");
           
        }

        [Test, Order(12)]
        public void FilteringByExistingDescriptionwithFormulaIDReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingDescriptionwithFormulaIDReturnsTheCorrectData("Rework");
           
        }

        [Test, Order(13)]
        public void FilteringByExistingDescriptionwithLocationReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingDescriptionwithLocationReturnsTheCorrectData("Rework");
           
        }

        [Test, Order(14)]
        public void FilteringByExistingDescriptionwithLotReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingDescriptionwithLotReturnsTheCorrectData("Rework");
           
        }

        [Test, Order(15)]
        public void FilteringByExistingFormulaIDReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FormulaIdFilterFunctionality("Rework");
           
        }

        [Test, Order(16)]
        public void FilteringByExistingFormulaIDwithBuildingReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingFormulaIDwithBuildingReturnsTheCorrectData("Rework");
           
        }

        [Test, Order(17)]
        public void FilteringByExistingFormulaIDwithContainerReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingFormulaIDwithContainerReturnsTheCorrectData("Rework");
           
        }

        [Test, Order(18)]
        public void FilteringByExistingFormulaIDwithLocationReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingFormulaIDwithLocationReturnsTheCorrectData("Rework");
           
        }

        [Test, Order(19)]
        public void FilteringByExistingLocationwithContainerReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingLocationwithContainerReturnsTheCorrectData("Rework");
           
        }

        [Test, Order(20)]
        public void FilteringByExistingLotReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.LotFilterFunctionality("Rework");
           
        }

        [Test, Order(21)]
        public void FilteringByExistingLotwithBuildingReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingLotwithBuildingReturnsTheCorrectData("Rework");
           
        }

        [Test, Order(22)]
        public void FilteringByExistingLotwithContainerReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingLotwithContainerReturnsTheCorrectData("Rework");
           
        }

        [Test, Order(23)]
        public void FilteringByExistingLotwithFormulaIdReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingLotwithFormulaIdReturnsTheCorrectData("Rework");
           
        }

        [Test, Order(24)]
        public void FilteringByExistingLotwithLocationReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingLotwithLocationReturnsTheCorrectData("Rework");
                     
        }

        [Test, Order(25)]
        public void FilteringByExistingSkuReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.SKUFilterFunctionality("Rework");
           
        }

        [Test, Order(26)]
        public void FilteringByExistingSkuwithBuildingReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingSkuwithBuildingReturnsTheCorrectData("Rework");
           
        }

        [Test, Order(27)]
        public void FilteringByExistingSkuwithContainerReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingSkuwithContainerReturnsTheCorrectData("Rework");
        }

        [Test, Order(28)]
        public void FilteringByExistingSkuwithDescriptionReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingSkuwithDescriptionReturnsTheCorrectData("Rework");
        }

        [Test, Order(29)]
        public void FilteringByExistingSkuwithFormulaIdReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingSkuwithFormulaIdReturnsTheCorrectData("Rework");
        }

        [Test, Order(30)]
        public void FilteringByExistingSkuwithLocationReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingSkuwithLocationReturnsTheCorrectData("Rework");
        }

        [Test, Order(31)]
        public void FilteringByExistingSkuwithLotReturnsTheCorrectData()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingSkuwithLotReturnsTheCorrectData("Rework");
            Pages.Rework.GotoHome();
            Pages.Login.Logout();
        }

        [Test, Order(32)]
        public void FilteringByExistingvalueandClearFilterReturnsAllDataVanish()
        {
            Pages.Rework.Goto();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByExistingvalueandClearFilterReturnsAllDataVanish("Rework");
            Pages.Rework.GotoHome();
            Pages.Login.Logout();
        }

        [Test, Order(33)]
        public void FilteringByNonExistingBuildingReturnsZeroRows()
        {
            Pages.Rework.Goto();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByNonExistingBuildingReturnsZeroRows("Rework");
        }
        
        [Test, Order(34)]
        public void FilteringByNonExistingLocationReturnsNoRecordsfoundMessage()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilteringByNonExistingLocationReturnsNoRecordsfoundMessage("Rework");
        }

        [Test, Order(35)]
        public void PaginationFunctionality()
        {
            Pages.Rework.Goto1();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.NavigatedToTheNextPage();
            Pages.Rework.GotoHome();
            Pages.Login.Logout();

        }

        [Test, Order(36)]
        public void RankingFunctionality()
        { 
            Pages.Login.LogIn("login"); 
            Pages.TopNavigation.ClickOverviewBtn();
            Pages.Rework.VerifyRankingFunctionality("Rework");
            System.Threading.Thread.Sleep(5000);
            Pages.Login.Logout();
        }

        [Test, Order(37)]
        public void RowCountDisplayasPerSearchedData()
        {
            Pages.Rework.Goto();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.AsPerSearchDataReturnRowCorret("Rework");
            Pages.Rework.GotoHome();
            Pages.Login.Logout();
        }

        [Test, Order(38)]
        public void TotalWeightOnReworkBtnAndReworkLookupPage()
        {
            Pages.Login.LogIn("login");
            Assert.IsTrue(Pages.TopNavigation.overviewBtn.Displayed);
            Pages.Rework.Reworkwt();
            Pages.Rework.GotoHome();
            Pages.Login.Logout();
           
        }

        [Test, Order(39)]
        public void TotalWeightasperSerchDataWeight()
        {
            Pages.Rework.Goto();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.GetTotalWeight("Rework");
            Pages.Rework.GotoHome();
            Pages.Login.Logout();
           
        } 
       
        [Test, Order(40)]
        public void VerifyRanking()
        {
            Pages.Rework.Goto();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.VerifyReworkRankingFunctionality("Rework");
            Pages.Rework.GotoHome();
            Pages.Login.Logout();

        }
    }
}