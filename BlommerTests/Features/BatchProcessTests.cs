﻿using BlommerFramework;
using NUnit.Framework;
using System;

namespace BlommerTests.Features
{
    [TestFixture]
    class BatchProcessTests : TestBase
    {
        private Boolean InitializeTestStartResult;


        [Test, Order(1)]
        public void InitializeTestData()
        {
            Browser.TestInitializerURL();
            Assert.IsTrue(Pages.Login.TestInitializerpage());
            InitializeTestStartResult = Pages.Login.InitializerTest();
          
        }


        [Test, Order(2)]
        public void LoginIntoApplication()
        {

            if (InitializeTestStartResult == false)
            {
                Assert.Ignore("Data not Initialize correctly");
            }
            Browser.Goto("");
            Assert.IsTrue(Pages.Login.IsAt());
            Assert.IsTrue(Browser.CurrentURL.Contains("http://bccqa-ws.ch.blommer.com/BatchProcess/Default.aspx"));
            Pages.Login.LogIn("login");
            Assert.IsTrue(Pages.BatchProcess.MainMenuBatchProcessPage());
        }

        [Test, Order(3)]
        public void CreateNewBatch()
        {
            if (InitializeTestStartResult == false)
            {
                Assert.Ignore("Data not Initialize correctly");
            }
            Pages.TopNavigation.ClickOverviewBtn();
            Assert.IsTrue(Pages.Overview.IsAt());
            Pages.BatchProcess.AvailableButtonwithandWithoutExistingBatch();
            Pages.BatchProcess.CreateNewBatch("BatchProcess");
        }

        [Test, Order(4)]
        public void AddRework()
        {
            if (InitializeTestStartResult == false)
            {
                Assert.Ignore("Data not Initialize correctly");
            }
            Assert.IsTrue(Pages.Overview.BatchMaintanancePage());
            Pages.BatchProcess.AddandRemoveRework("BatchProcess");

        }
        [Test, Order(5)]
        public void DistributeReworkIngredients()
        {
            if (InitializeTestStartResult == false)
            {
                Assert.Ignore("Data not Initialize correctly");
            }
            Pages.BatchProcess.DistributeReworkIngredients("BatchProcess");

        }

        [Test, Order(6)]
        public void ReleaseBatch()
        {
            if (InitializeTestStartResult == false)
            {
                Assert.Ignore("Data not Initialize correctly");
            }
            Pages.BatchProcess.BatchReleased("BatchProcess");

        }

        [Test, Order(7)]
        public void AddandRemoveReworkinPasteMixing()
        {
            if (InitializeTestStartResult == false)
            {
                Assert.Ignore("Data not Initialize correctly");
            }
            Pages.BatchProcess.AddandRemoveReworkinPasteMixing("BatchProcess");

        }

        [Test, Order(8)]
        public void AddandDeleteActualQuantitiesUsedinPasteMixing()
        {
            if (InitializeTestStartResult == false)
            {
                Assert.Ignore("Data not Initialize correctly");
            }
            Pages.BatchProcess.AddandDeleteActualQuantitiesUsedinPasteMixing("BatchProcess");

        }

        [Test, Order(9)]
        public void RefiningBatch()
        {
            if (InitializeTestStartResult == false)
            {
                Assert.Ignore("Data not Initialize correctly");
            }
            Pages.BatchProcess.Refining("BatchProcess");

        }

        [Test, Order(10)]
        public void DeleteBatch()
        {
            if (InitializeTestStartResult == false)
            {
                Assert.Ignore("Data not Initialize correctly");
            }
            Pages.BatchProcess.DeleteBatch("BatchProcess");

        }

        [Test, Order(11)]
        public void CleanUpTestData()
        {
            if (InitializeTestStartResult == false)
            {
                Assert.Ignore("Data not Initialize correctly");
            }
            Browser.TestInitializerURL();
            Assert.IsTrue(Pages.Login.TestInitializerpage());
            Pages.Login.CleanUpTest();
        }

    }
}