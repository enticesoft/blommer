﻿using BlommerFramework;
using NUnit.Framework;

namespace BlommerTests.Features
{
    [TestFixture]
    class LoginTests : TestBase
    {

        [Test, Order(1)]
        public void CanAccessLoginPage()

        {
            Assert.IsTrue(Pages.Login.IsAt());
            // TODO: Use constants for the URL
            Assert.IsTrue(Browser.CurrentURL.Equals(@"http://bccqa-ws.ch.blommer.com/BatchProcess/Default.aspx", System.StringComparison.OrdinalIgnoreCase));
        }

        [Test, Order(2)]

        public void CanLoginAsUser()

        {
            Pages.Login.LogIn("login");
            Assert.IsTrue(Pages.TopNavigation.overviewBtn.Displayed);
            // TODO: Use constants for the URL
            Assert.IsTrue(Browser.CurrentURL.Equals(@"http://bccqa-ws.ch.blommer.com/BatchProcess/BatchSystem/TaskList.aspx", System.StringComparison.OrdinalIgnoreCase));
        }

        [Test, Order(3)]

        public void CanLogoutUser()
        {

            Assert.IsTrue(Pages.TaskList.IsAt());
            Pages.Login.Logout();
            Assert.IsTrue(Pages.Login.IsAt());
          
        }

        [Test, Order(4)]
        public void LoginwithInvalidCredentials()
        {
            Pages.Login.InValidLogIn("login");
            Assert.IsTrue(Pages.Login.validationMsg.Displayed);

        }
        
    }
}
