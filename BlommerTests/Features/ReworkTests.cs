﻿using BlommerFramework;
using NUnit.Framework;

namespace BlommerTests.Features
{
    [TestFixture]
    class ReworkTests : TestBase
    {
        [Test, Order(1)]
        public void ApplyBuildingFilterFunctionality()
        {
            Pages.Rework.Goto();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.BuildingFilterFunctionality("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(2)]
        public void ApplyContainerFilterFunctionality()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.ContainerFilterFunctionality("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(3)]
        public void ApplyDescriptionFilterFunctionality()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.DescriptionFilterFunctionality("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(4)]
        public void ApplyFormulaIdFilterFunctionality()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FormulaIdFilterFunctionality("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(5)]
        public void ApplyLocationFilterFunctionality()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.LocationFilterFunctionality("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(6)]
        public void ApplyLotFilterFunctionality()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.LotFilterFunctionality("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(7)]
        public void ApplySKUFilterFunctionality()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.SKUFilterFunctionality("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(8)]
        public void AvailableContentsonReworkLookupPage()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Browser.ImplicitWiat();
            Pages.Rework.Contents();
            Browser.ImplicitWiat();
        }

        [Test, Order(9)]
        public void BuildingandContainerCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.Building_ContainerFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(10)]
        public void BuildingandLocationCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.Building_LocationFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(11)]
        public void CanAccessReworkLookupPage()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Browser.ImplicitWiat();
        }

        [Test, Order(12)]
        public void ClearFilterFunctionality()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.ClearFilter("Rework");
            Browser.ImplicitWiat();
            Pages.Rework.GotoHome();
            Pages.Login.Logout();
            Browser.ImplicitWiat();
        }

        [Test, Order(13)]
        public void ClickOnCloseBtnandNavOnHomePage()
        {
            Pages.Login.LogIn("login");
            Browser.ImplicitWiat();
            Pages.TopNavigation.ClickReworkBtn();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Assert.IsTrue(Pages.Rework.closeButton.Displayed);
            Pages.Rework.ClickCloseBtn();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.TaskList.IsAt());
            Assert.IsTrue(Pages.TopNavigation.reworkBtn.Displayed);
            Pages.Login.Logout();
            Browser.ImplicitWiat();
        }

        [Test, Order(14)]
        public void DescriptionandBuildingCombinationfilter()
        {
            Pages.Rework.Goto();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.Description_BuildingFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(15)]
        public void DescriptionandContainerCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.Description_ContainerFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(16)]
        public void DescriptionandFormulaIDCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.Description_FormulaIDFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(17)]
        public void DescriptionandLocationCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.Description_LocationFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(18)]
        public void DescriptionandLotCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.Description_LotFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(19)]
        public void FilterApplyAndVerifyContents()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FilterVerifyReworkLookupPageContents("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(20)]
        public void FormulaIDandBuildingCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FormulaID_BuildingFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(21)]
        public void FormulaIDandContainerCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FormulaID_ContainerFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(22)]
        public void FormulaIDandLocationCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.FormulaID_LocationFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(23)]
        public void LocationandContainerCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.Location_ContainerFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(24)]
        public void LotandBuildingCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.Lot_BuildingFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(25)]
        public void LotandContainerCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.Lot_ContainerFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(26)]
        public void LotandFormulaIDCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.Lot_FormulaIDFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(27)]
        public void LotandLocationCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.Lot_LocationFilterData("Rework");
            Browser.ImplicitWiat();
            Pages.Rework.GotoHome();
            Pages.Login.Logout();
            Browser.ImplicitWiat();
        }

        [Test, Order(28)]
        public void PaginationFunctionality()
        {
            Pages.Rework.Goto();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.CheckPagination();
            Browser.ImplicitWiat();
            Pages.Rework.GotoHome();
            Pages.Login.Logout();
            Browser.ImplicitWiat();
        }

        [Test, Order(29)]
        public void RankingFunctionality()
        {
            Pages.Login.LogIn("login");
            Browser.ImplicitWiat();
            Pages.TopNavigation.ClickOverviewBtn();
            Browser.ImplicitWiat();
            Pages.Rework.VerifyRankingFunctionality("Rework");
            Pages.Login.Logout();
            Browser.ImplicitWiat();
        }

        [Test, Order(30)]
        public void RowCountDisplayasPerSearchedData()
        {
            Pages.Rework.Goto();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.VerifyRowsCountAfterFilter("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(31)]
        public void SkuandBuildingCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.SKU_BuildingFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(32)]
        public void SkuandContainerCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.SKU_ContainerFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(33)]
        public void SkuandDescriptionCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.SKU_DescriptionFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(34)]
        public void SkuandFormulaIDCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.SKU_FormulaIDFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(35)]
        public void SkuandLocationCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.SKU_LocationFilterData("Rework");
            Browser.ImplicitWiat();
        }

        [Test, Order(36)]
        public void SkuandLotCombinationfilter()
        {
            Pages.Rework.Goto1();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.SKU_LotFilterData("Rework");
            Browser.ImplicitWiat();
            Pages.Rework.GotoHome();
            Browser.ImplicitWiat();
            Pages.Login.Logout();
            Browser.ImplicitWiat();
        }

        [Test, Order(37)]
        public void TotalWeightOnReworkBtnAndReworkLookupPage()
        {
            Pages.Login.LogIn("login");
            Assert.IsTrue(Pages.TopNavigation.overviewBtn.Displayed);
            Browser.ImplicitWiat();
            Pages.Rework.Reworkwt();
            Browser.ImplicitWiat();
            Pages.Rework.GotoHome();
            Pages.Login.Logout();
            Browser.ImplicitWiat();
        }

        [Test, Order(38)]
        public void TotalWeightasperSerchDataWeight()
        {
            Pages.Rework.Goto();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.Rework.IsAt());
            Pages.Rework.GetTotalWeight("Rework");
            Browser.ImplicitWiat();
            Pages.Rework.GotoHome();
            Pages.Login.Logout();
            Browser.ImplicitWiat();
        }

        [Test, Order(39)]
        public void VerifyFilterFormulaFunctioningAndAvialableContents()
        {
            Pages.Rework.VerifyFormulaId();
            Browser.ImplicitWiat();
            Pages.Rework.VerifyRowsCount();
           
        }

        [Test, Order(40)]
        public void ConfirmFilteringIsWorking()
        {
            Browser.LoginURL();
            Pages.Login.LogIn("login");
            Browser.ImplicitWiat();
            Pages.Rework.ClickReworkBtn();
            Browser.ImplicitWiat();
            Pages.Rework.ConfirmFilteringIsWorking("SmokeRework");
            Browser.ImplicitWiat();
            Pages.Rework.LotSearchResult();
            Browser.ImplicitWiat();
            Assert.IsTrue(Pages.TaskList.IsAt());
            Browser.ImplicitWiat();
            Pages.Login.Logout();
            Browser.ImplicitWiat();
        }

        [Test, Order(41)]
        public void VerifyRanking()
        {
            Pages.Login.LogIn("login");
            Browser.ImplicitWiat();
            Pages.TopNavigation.ClickOverviewBtn();
            Browser.ImplicitWiat();
            Pages.Rework.VerifyReworkRankingFunctionality("Rework");

        }

        [Test, Order(42)]
        public void AddReworkPriorToReleaseBatch()
        {
            Pages.Login.GotoLoginPage();
            Pages.Login.LogIn("login");
            Browser.ImplicitWiat();
            Pages.TopNavigation.ClickOverviewBtn();
            Browser.ImplicitWiat();
            Pages.Rework.AddRework("Rework");
            Pages.Login.Logout();
            Browser.ImplicitWiat();

        }
        [Test, Order(43)]

        public void ReleaseBatch()
        {
            Pages.Login.GotoLoginPage();
            Pages.Login.LogIn("login");
            Browser.ImplicitWiat();
            Pages.TopNavigation.ClickOverviewBtn();
            Browser.ImplicitWiat();
            Pages.Rework.ReleaseBatch("SmokeRework");

        }

        [Test, Order(44)]

        public void ReworkInPasteMixing()
        {
            Pages.Login.GotoLoginPage();
            Pages.Login.LogIn("login");
            Browser.ImplicitWiat();
            Pages.Rework.ReworkPasteMixing("SmokeRework");
            Browser.ImplicitWiat();
            Pages.Login.Logout();
            Browser.ImplicitWiat();

        }

        [Test, Order(45)]

        public void ReworkInStabilizing()
        {
            Pages.Login.GotoLoginPage();
            Pages.Login.LogIn("login");
            Browser.ImplicitWiat();
            Pages.Rework.ReworkStabilizing("SmokeRework");
            Browser.ImplicitWiat();
            Pages.Login.Logout();
            Browser.ImplicitWiat();

        }


        [Test, Order(46)]

        public void DeleteRework()
        {
            Pages.Login.GotoLoginPage();
            Pages.Login.LogIn("login");
            Browser.ImplicitWiat();
            Pages.TopNavigation.ClickOverviewBtn();
            Browser.ImplicitWiat();
            Pages.Rework.UnReleaseBatch("SmokeRework");
            Browser.ImplicitWiat();
            Pages.Rework.DeleteRework("SmokeRework");
            Browser.ImplicitWiat();
            Pages.Login.Logout();
            Browser.ImplicitWiat();

        }
    }
}